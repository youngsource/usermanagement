<?php
declare(strict_types=1);

use function Laudis\UserManagement\clear_cache;

require __DIR__ . '/../../vendor/autoload.php';

clear_cache();

echo 'Cache cleared';
