<?php /** @noinspection PhpUnhandledExceptionInspection */

use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Databags\AppConfig;
use Laudis\UserManagement\Databags\User;
use Laudis\UserManagement\JWS\JWSFactory;

use Laudis\UserManagement\UserPresenter;
use function DI\decorate;
use function Laudis\UserManagement\Tests\make_container;

include_once __DIR__ . '/../../vendor/autoload.php';

/** @var JWSFactory $factory */
$container = make_container();
$container->set(
    AppConfig::class,
    decorate(
        static function (AppConfig $config) {
            return new AppConfig(
                'development',
                $config->getPrivateKey(),
                $config->getPublicKey(),
                $config->isAuthOn(),
                $config->getAuthPulicKey(),
                $config->getMode(),
                $config->getAudiences(),
                $config->getAudience(),
                $config->getAppDomain()
            );
        }
    )
);

$factory = $container->get(JWSFactory::class);
$presenter = $container->get(UserPresenter::class);

$dev = new User(1, 'dev@laudis.tech', new DateTime(), new DateTime(), 'dev', 'abc');
echo $factory->make($dev, 'development', $presenter->devToken($container->get(PasswordHasherInterface::class)));

echo "\n";

echo $factory->make($dev, 'development', $presenter->presentForId($dev));
