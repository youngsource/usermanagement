<?php

declare(strict_types=1);

use function Laudis\UserManagement\generate_public_private_key;

require __DIR__ . '/../../vendor/autoload.php';

[$public, $private] = generate_public_private_key();

echo "$public
$private
";


