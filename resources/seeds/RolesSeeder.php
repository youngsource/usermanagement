<?php
declare(strict_types=1);


use Faker\Factory;
use Laudis\UserManagement\Contracts\RolesRepositoryInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Phinx\Seed\AbstractSeed;
use function Laudis\UserManagement\Tests\make_container;

final class RolesSeeder extends AbstractSeed
{
    /** @noinspection PhpUnhandledExceptionInspection */
    public function run(): void
    {
        parent::run();

        $faker = Factory::create();
        $container = make_container();
        /** @var RolesRepositoryInterface $rolesRepo */
        $rolesRepo = $container->get(RolesRepositoryInterface::class);
        for ($i = 0; $i < 3; ++$i) {
            $rolesRepo->insertRole($faker->name);
        }
        $rolesRepo->insertRole('guest');
        $rolesRepo->insertRole('admin');

        $users = $container->get(UserRepositoryInterface::class)->getAllUsers();
        $roles = $rolesRepo->getAllRoles();

        foreach ($users as $user) {
            $rolesRepo->syncRole($user, $roles[$faker->numberBetween(0, count($roles) - 1)]);
        }
    }

    public function getDependencies(): array
    {
        return array_merge(parent::getDependencies(), [UserSeeder::class]);
    }
}