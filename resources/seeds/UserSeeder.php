<?php
declare(strict_types=1);


use DI\DependencyException;
use DI\NotFoundException;
use Faker\Factory;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Phinx\Seed\AbstractSeed;

use function Laudis\UserManagement\Tests\make_container;


final class UserSeeder extends AbstractSeed
{
    /**
     * @throws DependencyException
     * @throws NotFoundException
     * @throws Exception
     */
    public function run(): void
    {
        parent::run();
        $faker = Factory::create();
        $container = make_container();
        $repo = $container->get(UserRepositoryInterface::class);
        $hasher = $container->get(PasswordHasherInterface::class);

        for ($i = 0; $i < 10; ++$i) {
            $repo->insertUser($faker->email, $faker->userName, $hasher->hash($faker->password));
        }
    }
}
