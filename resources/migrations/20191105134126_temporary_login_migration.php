<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

/** @noinspection AutoloadingIssuesInspection */

final class TemporaryLoginMigration extends AbstractMigration
{
    /**
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function change(): void
    {
        $this->table('temporary_logins')
            ->addTimestamps()
            ->addColumn('viewed_intro_message', 'boolean')
            ->addIndex('viewed_intro_message')
            ->addColumn('valid_until', 'datetime')
            ->addIndex('valid_until')
            ->addColumn('user_id', 'integer')
            ->addForeignKey('user_id', 'users', 'id')
            ->create();
    }
}
