<?php
/** @noinspection AutoloadingIssuesInspection */
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class RequestPoliciesMigration extends AbstractMigration
{
    /**
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function change(): void
    {
        $this->table('request_policies')
            ->addColumn('role_id', 'integer')
            ->addColumn('act', 'string')
            ->addColumn('path', 'text')
            ->addColumn('data', 'text')
            ->addForeignKey('role_id', 'roles', 'id')
            ->addIndex('act')
            ->addIndex('path')
            ->create();
    }
}
