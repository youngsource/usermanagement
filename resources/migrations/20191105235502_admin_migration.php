<?php /** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

/** @noinspection AutoloadingIssuesInspection */

final class AdminMigration extends AbstractMigration
{
    public function up(): void
    {
        $admins = $this->adapter->query(
            "SELECT * FROM users WHERE email IN (
            'ghlen@pm.me', 
            'tim.galloo@gmail.com', 
            'anja.berlanger@minfin.fed.be',
            'Annik.cardoen@galloptaxshelter.be',
            'Kristel.romulus@galloptaxshelter.be',
            'Marcel.zandstra@galloptaxshelter.be',
            'Lieve.decoster@demensen.be',
            'Maurits.lemmens@demensen.be',
            'Guy.torrekens@demensen.be',
            'Mike.lammers@demensen.be',
            'Karen.feys@demensen.be'
       )"
        )->fetchAll();

        $this->adapter->insert($this->table('roles')->getTable(), ['name' => 'admin', 'id' => 1]);
        $roleId = $this->adapter->query("SELECT * FROM roles WHERE name = 'admin'")->fetchAll()[0]['id'];

        foreach ($admins as $admin) {
            $this->adapter->insert(
                $this->table('user_roles')->getTable(),
                [
                    'role_id' => $roleId,
                    'user_id' => $admin['id'],
                ]
            );
        }
    }

    public function down(): void
    {
    }
}
