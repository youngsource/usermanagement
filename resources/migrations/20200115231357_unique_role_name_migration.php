<?php /** @noinspection AutoloadingIssuesInspection */
/** @noinspection PhpUnhandledExceptionInspection */
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class UniqueRoleNameMigration extends AbstractMigration
{
    public function up(): void
    {
        $this->table('roles')
            ->removeIndex('name')
            ->addIndex('name', ['unique' => true])
            ->update();

        $this->table('user_roles')
            ->addIndex(['user_id', 'role_id'], ['unique' => true])
            ->update();

        $this->table('user_roles')
            ->dropForeignKey('role_id')
            ->addForeignKey('role_id', 'roles', 'id', ['delete' => 'CASCADE', 'update' => 'CASCADE'])
            ->update();
    }

    public function down(): void
    {
    }
}
