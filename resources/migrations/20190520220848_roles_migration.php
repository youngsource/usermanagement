<?php /** @noinspection PhpUnhandledExceptionInspection */
/** @noinspection AutoloadingIssuesInspection */

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

/**
 * @suppress PhanRedefineClass
 */
final class RolesMigration extends AbstractMigration
{
    public function change(): void
    {
        $this->table('roles')
            ->addTimestamps()
            ->addColumn('name', 'string')
            ->addIndex('name')
            ->create();

        $this->table('user_roles')
            ->addTimestamps()
            ->addColumn('user_id', 'integer')
            ->addColumn('role_id', 'integer')
            ->addForeignKey('user_id', 'users', 'id')
            ->addForeignKey('role_id', 'roles', 'id')
            ->create();
    }
}
