<?php /** @noinspection PhpUnhandledExceptionInspection */

/** @noinspection AutoloadingIssuesInspection */

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class UsersMigration extends AbstractMigration
{
    public function change(): void
    {
        $this->table('users')
            ->addColumn('email', 'string')
            ->addTimestamps()
            ->addColumn('username', 'string')
            ->addColumn('password_hash', 'string')
            ->addIndex('email', ['unique' => true])
            ->addIndex('password_hash')
            ->create();
    }
}
