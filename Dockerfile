FROM php:7.4-fpm

LABEL maintainer="Ghlen Nagels <ghlen@pm.me>"

RUN apt-get -yqq update \
    && apt-get install -yqq --no-install-recommends libzip-dev zip unzip ftp libicu-dev pandoc libgmp-dev libtidy-dev libpng-dev dos2unix \
    && docker-php-ext-install -j$(nproc) pdo_mysql opcache zip intl bcmath pcntl gmp tidy gd \
    && pecl install redis ds apcu ast \
    && docker-php-ext-enable redis ds apcu ast

ARG WITH_XDEBUG=false

RUN if [ $WITH_XDEBUG = "true" ] ; then \
        pecl install xdebug; \
        docker-php-ext-enable xdebug; \
fi;


#COPY docker/composer-installer.sh /usr/local/bin/composer-installer
COPY --from=composer /usr/bin/composer /usr/bin/composer

COPY docker/php.ini /usr/local/etc/php/php.ini
COPY docker/conf/* /usr/local/etc/php/conf.d/

#apparently the home directory of www-data is not owned by www-data.. only $home/html is owned by www-data
RUN mkdir /var/www/logs $(eval echo "~www-data")/.composer \
    && chown www-data:www-data /var/www/logs $(eval echo "~www-data")/.composer

RUN chown www-data:www-data /var/www

USER www-data:www-data

RUN composer global require hirak/prestissimo --no-plugins --no-scripts \
    && mkdir caching api
WORKDIR caching
COPY composer.json composer.loc[k] ./
RUN composer install \
        --no-interaction \
        --prefer-dist \
        --no-progress \
        --no-scripts \
        --profile \
        --no-autoloader

WORKDIR /var/www/html/api/
COPY --chown=www-data:www-data resources/ resources/
COPY --chown=www-data:www-data public/ public/
COPY --chown=www-data:www-data src/ src/
COPY --chown=www-data:www-data composer.json composer.loc[k] .env .php_cs.dist .phpcs.xml phinx.php phpunit.xml ./
COPY --chown=www-data:www-data tests/ tests/
COPY --chown=www-data:www-data temp/ temp/

RUN composer install \
        --no-interaction \
        --prefer-dist \
        --no-progress \
        --no-scripts \
        --profile

