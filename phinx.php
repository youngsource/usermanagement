<?php /** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use DI\ContainerBuilder;

require_once __DIR__ . '/vendor/autoload.php';

$builder = new ContainerBuilder();
$builder->addDefinitions(__DIR__ . '/vendor/laudis/common/src/register.php');
$container = $builder->build();
$container->set('project_root', __DIR__);
$pdo = $container->get(PDO::class);

return [
    'paths' => [
        'migrations' => __DIR__ . '/resources/migrations',
        'seeds' => __DIR__ . '/resources/seeds',
    ],
    'environments' => [
        'default_migration_table' => 'migrations_log',
        'default_database' => 'default',
        'default' => [
            'adapter' => 'mysql',
            'connection' => $pdo,
            'name' => $pdo->query('SELECT DATABASE()')->fetchColumn(),
        ],
    ],
    'version_order' => 'creation',
];
