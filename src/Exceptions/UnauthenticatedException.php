<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Exceptions;

use Laudis\Common\Enums\HTTPStatusCodes\ClientErrorEnum;
use RuntimeException;
use Throwable;


final class UnauthenticatedException extends RuntimeException
{
    public function __construct(string $message = '', int $code = null, Throwable $previous = null)
    {
        parent::__construct($message, $code ?? (int) ClientErrorEnum::UNAUTHORIZED()->getValue(), $previous);
    }
}
