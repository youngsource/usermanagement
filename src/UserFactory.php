<?php

declare(strict_types=1);

namespace Laudis\UserManagement;

use DateTime;
use Exception;
use Laudis\UserManagement\Databags\GuestLogin;
use Laudis\UserManagement\Databags\Role;
use Laudis\UserManagement\Databags\User;


final class UserFactory
{
    /**
     * Makes a user from an associative array.
     * Accepted values are:
     *  - id: int
     *  - email: string
     *  - created_at: string
     *  - ?updated_at: string|null
     *  - username: string
     *  - password_hash: string
     * @throws Exception
     */
    public function userFromAssociativeArray(array $array): User
    {
        return new User(
            (int) $array['id'],
            (string) $array['email'],
            new DateTime($array['created_at']),
            $array['updated_at'] === null ? null : new DateTime($array['updated_at']),
            $array['username'],
            $array['password_hash'] ?? 'REDACTED'
        );
    }

    /**
     * Makes a role from an associative array.
     * Accepted values are:
     *  - id: int
     *  - name: string
     */
    public function roleFromAssociativeArray(array $array): Role
    {
        return new Role((string) $array['name'], (int) $array['id']);
    }

    /**
     * Makes a guest value from an associative array.
     * Accepted values are:
     *  - valid_until: string
     *  - viewed_intro_message: bool
     *
     * @throws Exception
     */
    public function guestLoginFromAssociativeArray(array $result): GuestLogin
    {
        return new GuestLogin(new DateTime($result['valid_until']), (bool) $result['viewed_intro_message']);
    }
}
