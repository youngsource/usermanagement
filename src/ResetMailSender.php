<?php
declare(strict_types=1);


namespace Laudis\UserManagement;


use Laudis\Common\MailSender;
use Laudis\UserManagement\Databags\AppConfig;
use Mailgun\Exception\HttpServerException;
use Mailgun\Exception\InvalidArgumentException;
use Mailgun\Model\Message\SendResponse;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;

final class ResetMailSender
{
    private MailSender $mailSender;
    private AppConfig $config;

    public function __construct(MailSender $mailSender, AppConfig $config)
    {
        $this->mailSender = $mailSender;
        $this->config = $config;
    }

    /**
     * @return SendResponse|ResponseInterface
     * @throws \InvalidArgumentException
     * @throws HttpServerException
     * @throws InvalidArgumentException
     * @throws ClientExceptionInterface
     *
     * @throws RuntimeException
     */
    public function send(string $email, string $token)
    {
        $url = $this->config->getAppDomain() . '/reset-password/' . $token;

        return $this->mailSender->send(
            $email,
            'Uw passwoord reset link van: ' . $this->config->getName(),
            reset_password_email($url)
        );
    }
}
