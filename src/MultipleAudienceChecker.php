<?php
declare(strict_types=1);


namespace Laudis\UserManagement;


use Jose\Component\Checker\ClaimChecker;
use Jose\Component\Checker\HeaderChecker;
use Jose\Component\Checker\InvalidClaimException;
use Jose\Component\Checker\InvalidHeaderException;
use Laudis\UserManagement\Databags\AppConfig;

use function in_array;
use function is_array;
use function is_string;

final class MultipleAudienceChecker implements ClaimChecker, HeaderChecker
{
    private const CLAIM_NAME = 'aud';

    private bool $protectedHeader;
    private AppConfig $config;

    public function __construct(AppConfig $config, bool $protectedHeader = false)
    {
        $this->protectedHeader = $protectedHeader;
        $this->config = $config;
    }

    /**
     * @param mixed $value
     * @throws InvalidClaimException
     * @throws InvalidHeaderException
     */
    public function checkClaim($value): void
    {
        $this->checkValue($value, InvalidClaimException::class);
    }

    /**
     * @inheritDoc
     */
    public function supportedClaim(): string
    {
        return self::CLAIM_NAME;
    }

    /**
     * @param mixed $value
     *
     * @throws InvalidClaimException
     * @throws InvalidHeaderException
     */
    public function checkHeader($value): void
    {
        $this->checkValue($value, InvalidHeaderException::class);
    }

    /**
     * @inheritDoc
     */
    public function supportedHeader(): string
    {
        return self::CLAIM_NAME;
    }

    /**
     * @inheritDoc
     */
    public function protectedHeaderOnly(): bool
    {
        return $this->protectedHeader;
    }

    /**
     * @param mixed $value
     *
     * @throws InvalidClaimException
     * @throws InvalidHeaderException
     */
    private function checkValue($value, string $class): void
    {
        if (is_string($value) && ($this->isInvalidAudience($value) || $this->isInvalidDevAudience($value))) {
            throw new $class('Bad audience.', self::CLAIM_NAME, $value);
        }

        if (is_array($value)) {
            foreach ($value as $item) {
                $this->checkValue($item, $class);
            }
        }

        if (!is_array($value) && !is_string($value)) {
            throw new $class('Bad audience.', self::CLAIM_NAME, $value);
        }
    }

    private function isInvalidAudience($value): bool
    {
        $audiences = array_merge($this->config->getAudiences(), ['all']);
        return !in_array($value, $audiences, true);
    }

    private function isInvalidDevAudience($value): bool
    {
        return $value === 'development' && $this->config->getMode() !== 'local';
    }
}
