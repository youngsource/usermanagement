<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Repositories;

use DateTimeInterface;
use Exception;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Databags\GuestLogin;
use Laudis\UserManagement\Databags\PasswordReset;
use Laudis\UserManagement\Databags\User;
use Laudis\UserManagement\UserFactory;
use PDO;
use PDOException;
use PDOStatement;

use function array_map;
use function compact;

final class UserRepository implements UserRepositoryInterface
{
    private PDO $pdo;
    private UserFactory $userFactory;

    public function __construct(PDO $pdo, UserFactory $userFactory)
    {
        $this->pdo = $pdo;
        $this->userFactory = $userFactory;
    }

    /**
     * @throws PDOException
     */
    public function insertUser(string $email, string $username, string $passwordHash): void
    {
        $this->execute(
            '
            INSERT INTO users (email, username, password_hash) 
            VALUES (:email, :username, :passwordHash)
        ',
            compact('email', 'username', 'passwordHash')
        );
    }

    /**
     * @throws PDOException
     * @throws Exception
     */
    public function findUser(string $email): ?User
    {
        $result = $this->execute('SELECT * FROM users WHERE email = :email', ['email' => $email])->fetch();
        if ($result === false) {
            return null;
        }

        return $this->parseResult($result);
    }

    public function updateUser(User $user): void
    {
        $email = $user->getEmail();
        $name = $user->getUserName();
        $hash = $user->getPasswordHash();
        $id = $user->getId();
        $this->execute(
            '
            UPDATE users 
            SET email = :email, username = :name, password_hash = :hash, updated_at = current_timestamp() 
            WHERE id = :id
        ',
            compact('email', 'name', 'hash', 'id')
        );
    }

    /**
     * @return User[]
     * @suppress PhanPossiblyFalseTypeArgumentInternal
     * @throws PDOException
     * @throws Exception
     */
    public function getAllUsers(int $limit = 200, int $offset = 0): array
    {
        $result = $this->execute("SELECT * FROM users LIMIT $offset,$limit", [])->fetchAll();

        return array_map(fn(array $values) => $this->userFactory->userFromAssociativeArray($values), $result);
    }

    /**
     * @param User $user
     * @suppress PhanPluginUseReturnValueInternalKnown
     * @suppress PhanPossiblyNonClassMethodCall
     * @throws PDOException
     */
    public function deleteUser(User $user): void
    {
        $id = $user->getId();
        $statement = $this->pdo->prepare('DELETE FROM users WHERE id = :id');
        $statement->execute(['id' => $id]);
    }

    /**
     * @throws Exception
     */
    public function getUserById(int $id): ?User
    {
        $result = $this->execute('SELECT * FROM users WHERE id = :id', ['id' => $id])->fetch();
        if ($result === false) {
            return null;
        }

        return $this->parseResult($result);
    }

    /**
     * @throws Exception
     */
    public function getGuestLogin(User $user): ?GuestLogin
    {
        $statement = $this->execute('SELECT * FROM temporary_logins WHERE user_id = :id', ['id' => $user->getId()]);
        $result = $statement->fetch();

        if ($result !== false) {
            return $this->userFactory->guestLoginFromAssociativeArray($result);
        }

        return null;
    }

    /**
     * @throws Exception
     */
    public function makeOrUpdateGuestLogin(User $user, DateTimeInterface $validUntil): void
    {
        $login = $this->getGuestLogin($user);
        $params = ['id' => $user->getId(), 'date' => $validUntil->format('Y-m-d H:i:s')];
        if ($login === null) {
            $this->execute(
                'INSERT INTO temporary_logins (`viewed_intro_message`, `user_id`, `valid_until`) VALUES 
                                                                                       (false, :id, :date)',
                $params
            );
        } else {
            $this->execute('UPDATE temporary_logins SET `valid_until`=:date WHERE user_id = :id', $params);
        }
    }

    /**
     * @throws Exception
     */
    public function addPasswordReset(User $user, string $token): void
    {
        $this->execute(
            '
            INSERT INTO password_resets (token, user_id) VALUES (:token, (
                SELECT id FROM users WHERE email = :email)
            )
        ',
            ['token' => $token, 'email' => $user->getEmail()]
        );
    }

    /**
     * @throws Exception
     */
    public function usePasswordResets(User $user): void
    {
        $this->execute('UPDATE password_resets SET `used` = 1 WHERE `user_id` = :id', ['id' => $user->getId()]);
    }

    public function setUserSeenMessage(User $user, bool $seen): void
    {
        $value = $seen ? 1 : 0;
        $id = $user->getId();
        $this->execute(
            'UPDATE temporary_logins SET `viewed_intro_message` = :value WHERE `user_id` = :id',
            compact('value', 'id')
        );
    }

    /**
     * @return PasswordReset[]
     * @suppress PhanPossiblyFalseTypeArgumentInternal
     * @throws PDOException
     */
    public function getPasswordResets(User $user): array
    {
        $statement = $this->pdo->prepare('SELECT token, used FROM password_resets WHERE user_id = :id LIMIT 200');
        $statement->execute(['id' => $user->getId()]);
        return array_map(
            static fn(array $val) => new PasswordReset($val['token'], (bool)$val['used']),
            $statement->fetchAll()
        );
    }

    /**
     * @throws PDOException
     */
    private function execute(string $statement, array $params): PDOStatement
    {
        $stmnt = $this->pdo->prepare($statement);
        $stmnt->execute($params);
        return $stmnt;
    }

    /**
     * @throws Exception
     */
    private function parseResult(array $result): User
    {
        return $this->userFactory->userFromAssociativeArray($result);
    }
}
