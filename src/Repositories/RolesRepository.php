<?php
declare(strict_types=1);


namespace Laudis\UserManagement\Repositories;


use Laudis\UserManagement\Contracts\RolesRepositoryInterface;
use Laudis\UserManagement\Databags\Role;
use Laudis\UserManagement\Databags\User;
use Laudis\UserManagement\UserFactory;
use PDO;
use PDOException;
use PDOStatement;

use function array_map;
use function is_array;

final class RolesRepository implements RolesRepositoryInterface
{
    private PDO $pdo;
    private UserFactory $factory;

    public function __construct(PDO $pdo, UserFactory $factory)
    {
        $this->pdo = $pdo;
        $this->factory = $factory;
    }

    public function getRolesForUser(User $user): array
    {
        $statement = $this->execute(
            'SELECT * FROM roles WHERE id IN  (SELECT role_id FROM user_roles WHERE user_id = :id)',
            ['id' => $user->getId()]
        );
        $result = $statement->fetchAll();

        return array_map(fn(array $values) => $this->factory->roleFromAssociativeArray($values), $result);
    }

    public function syncRole(User $user, Role $role): void
    {
        $column = $this->execute(
            'SELECT id FROM user_roles WHERE role_id = :roleId AND user_id = :userId',
            ['roleId' => $role->getId(), 'userId' => $user->getId()]
        )->fetchColumn();

        if ($column === false) {
            $this->execute(
                'INSERT INTO user_roles (role_id, user_id) VALUES (:roleId, :userId)',
                ['roleId' => $role->getId(), 'userId' => $user->getId()]
            );
        }
    }

    public function detachRole(User $user, Role $role): void
    {
        $this->execute(
            'DELETE FROM user_roles WHERE user_id = :userId AND role_id = :roleId',
            ['roleId' => $role->getId(), 'userId' => $user->getId()]
        );
    }

    public function insertRole(string $role): Role
    {
        $this->execute('INSERT INTO roles (name) VALUES (:name)', ['name' => $role]);

        return $this->factory->roleFromAssociativeArray(['name' => $role, 'id' => $this->pdo->lastInsertId()]);
    }

    /**
     * @throws PDOException
     */
    public function getAllRoles(int $limit = 200, int $offset = 0): array
    {
        $statement = $this->execute("SELECT * FROM roles LIMIT $limit OFFSET $offset", []);

        $result = $statement->fetchAll();

        return array_map(fn(array $values) => $this->factory->roleFromAssociativeArray($values), $result);
    }

    public function getRole(string $role): ?Role
    {
        $result = $this->execute('SELECT * FROM roles WHERE name = :name', ['name' => $role])->fetch();
        if (!is_array($result)) {
            return null;
        }
        return $this->factory->roleFromAssociativeArray($result);
    }

    public function deleteRole(Role $role): void
    {
        $this->execute('DELETE FROM roles WHERE id = :id', ['id' => $role->getId()]);
    }

    /**
     */
    private function execute(string $statement, array $params): PDOStatement
    {
        $stmnt = $this->pdo->prepare($statement);
        /** @noinspection PhpUnhandledExceptionInspection */
        $stmnt->execute($params);
        return $stmnt;
    }
}
