<?php

declare(strict_types=1);

namespace Laudis\UserManagement;

use Casbin\Enforcer;
use Casbin\Model\Assertion;
use DateTime;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\RolesRepositoryInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Databags\User;
use PDOException;
use function time;
use const PHP_INT_MAX;

final class UserPresenter
{
    private UserRepositoryInterface $repository;
    private RolesRepositoryInterface $rolesRepository;
    private Enforcer $enforcer;

    public function __construct(
        UserRepositoryInterface $repository,
        RolesRepositoryInterface $rolesRepository,
        Enforcer $enforcer
    )
    {
        $this->repository = $repository;
        $this->rolesRepository = $rolesRepository;
        $this->enforcer = $enforcer;
    }

    /**
     * @throws PDOException
     */
    public function present(User $user): array
    {
        $temporaryLogin = $this->repository->getGuestLogin($user);

        $presentedRoles = $this->roles($user);

        $dateTime = $user->getUpdatedAt();
        return [
            'email' => $user->getEmail(),
            'id' => $user->getId(),
            'username' => $user->getUserName(),
            'roles' => $presentedRoles,
            'createdAt' => $user->getCreatedAt()->getTimestamp(),
            'updatedAt' => $dateTime === null ? null : $dateTime->getTimestamp(),
            'guestLogin' => $temporaryLogin === null ? null : [
                'validUntil' => $temporaryLogin->getValidUntil()->format('Y-m-d'),
                'seenIntroMessage' => $temporaryLogin->seenIntroMessage(),
            ],
            'authorization' => $this->loadPolicies()
        ];
    }

    public function devToken(PasswordHasherInterface $hasher): array {
        return [
            'exp' => DateTime::createFromFormat('Y-m-d', '9999-12-31')->getTimestamp(),
            'type' => 'auth',
            'email' => 'dev@laudis.tech',
            'username' => 'development',
            'roles' => [
                ['name' => 'development', 'id' => 1]
            ],
            'created_at' => '@' . time(),
            'updated_at' => '@' . time(),
            'password_hash' => $hasher->hash('dev'),
            'guest_login' => null
        ];
    }

    /**
     * @throws PDOException
     */
    public function presentForToken(User $user): array
    {
        $temporaryLogin = $this->repository->getGuestLogin($user);

        $presentedRoles = $this->roles($user);

        $dateTime = $user->getUpdatedAt();
        return [
            'email' => $user->getEmail(),
            'username' => $user->getUserName(),
            'roles' => $presentedRoles,
            'created_at' => '@' . $user->getCreatedAt()->getTimestamp(),
            'updated_at' => $dateTime === null ? null : '@' . $dateTime->getTimestamp(),
            'guest_login' => $temporaryLogin === null ? null : [
                'valid_until' => $temporaryLogin->getValidUntil()->format('Y-m-d'),
                'seen_intro_message' => $temporaryLogin->seenIntroMessage(),
            ],
            'authorization' => $this->loadPolicies()
        ];
    }

    private function loadPolicies(): array
    {
        $model = $this->enforcer->getModel();
        /** @var Assertion $p */
        $p = $model['p']['p'] ?? new Assertion();
        return $p->policy;
    }

    public function presentForId(User $user): array {
        return [
            'type' => 'laudis-authentication',
            'iat' => 0,
            'nbf' => 0,
            'exp' => PHP_INT_MAX,
            'iss' => 'laudis',
            'aud' => 'all',
            'user_id' => $user->getId(),
            'jti' => $user->getId()
        ];
    }

    /**
     * @throws PDOException
     */
    private function roles(User $user): array
    {
        $roles = $this->rolesRepository->getRolesForUser($user);
        $presentedRoles = [];
        foreach ($roles as $role) {
            $presentedRoles[] = [
                'name' => $role->getName(),
                'id' => $role->getId(),
            ];
        }
        return $presentedRoles;
    }
}
