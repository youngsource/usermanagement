<?php

declare(strict_types=1);

namespace Laudis\UserManagement;

use JsonException;
use Laudis\Common\Exceptions\ValidationException;
use Rakit\Validation\Validation;
use Slim\Error\AbstractErrorRenderer;
use Throwable;

use function get_class;
use function json_encode;
use function safe_json_encode;

use const JSON_PRETTY_PRINT;
use const JSON_THROW_ON_ERROR;
use const JSON_UNESCAPED_SLASHES;

final class CustomJsonErrorRenderer extends AbstractErrorRenderer
{
    /**
     * @throws JsonException
     */
    public function __invoke(Throwable $exception, bool $displayErrorDetails): string
    {
        if ($exception instanceof ValidationException) {
            return safe_json_encode(
                [
                    'message' => $exception->getMessage(),
                    'errors' => $this->presentErrors($exception->getValidation()),
                ],
                JSON_PRETTY_PRINT
            );
        }
        return $this->basicException($exception, $displayErrorDetails);
    }

    /**
     * @throws JsonException
     */
    private function basicException(Throwable $exception, bool $displayErrorDetails): string
    {
        $error = ['message' => $exception->getMessage()];

        if ($displayErrorDetails) {
            $error['exception'] = [];
            do {
                $error['exception'][] = $this->formatExceptionFragment($exception);
                $exception = $exception->getPrevious();
            } while ($exception !== null);
        }

        return (string)json_encode($error, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }

    private function presentErrors(Validation $validation): array
    {
        $tbr = [];
        foreach ($validation->errors()->toArray() as $wrongKey => $reasons) {
            $tbr[$wrongKey] = [];
            foreach ($reasons as $reason) {
                $tbr[$wrongKey][] = $reason;
            }
        }

        return $tbr;
    }

    private function formatExceptionFragment(Throwable $exception): array
    {
        return [
            'type' => get_class($exception),
            'code' => $exception->getCode(),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'trace' => $exception->getTrace(),
        ];
    }
}
