<?php

declare(strict_types=1);

namespace Laudis\UserManagement;

use Laudis\UserManagement\Contracts\PasswordHasherInterface;

use function password_verify;

use const PASSWORD_ARGON2ID;

final class PasswordHasher implements PasswordHasherInterface
{
    /**
     * @suppress PhanPartialTypeMismatchReturn
     */
    public function hash(string $password, array $options = []): string
    {
        return password_hash($password, PASSWORD_ARGON2ID, $options);
    }

    public function check(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }
}
