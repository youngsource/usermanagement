<?php
declare(strict_types=1);


namespace Laudis\UserManagement;


use Casbin\Log\Logger;
use Psr\Log\LoggerInterface;
use function is_array;
use function is_object;
use JsonException;

final class CasbinPsrLogger implements Logger
{
    private LoggerInterface $logger;
    private bool $enabled;

    public function __construct(LoggerInterface $logger, bool $enabled = true)
    {
        $this->logger = $logger;
        $this->enabled = $enabled;
    }

    public function enableLog(bool $enable): void
    {
        $this->enabled = $enable;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @inheritDoc
     * @throws JsonException
     */
    public function write(...$v): void
    {
        if ($this->isEnabled()) {
            $content = [];
            foreach ($v as $value) {
                if (is_array($value) || is_object($value)) {
                    $value = json_encode($value, JSON_THROW_ON_ERROR);
                }
                $content .= $value;
            }
            $this->logger->info($content);
        }
    }

    /**
     * @inheritDoc
     */
    public function writef(string $format, ...$v): void
    {
        if ($this->isEnabled()) {
            $this->logger->info(sprintf($format, ...$v));
        }
    }
}