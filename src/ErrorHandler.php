<?php

declare(strict_types=1);

namespace Laudis\UserManagement;

use Laudis\Common\Exceptions\ValidationException;
use Laudis\UserManagement\Exceptions\UnauthenticatedException;
use Laudis\UserManagement\Exceptions\UnauthorizedException;

use function is_a;

final class ErrorHandler extends \Slim\Handlers\ErrorHandler
{
    private const CODE_WHITELIST = [
        UnauthenticatedException::class,
        UnauthorizedException::class,
        ValidationException::class,
    ];

    protected function logError(string $error): void
    {
        $code = $this->determineStatusCode();
        if ($code >= 500 || $code === 0) {
            parent::logError($error);
            $this->logger->error($error);
        } else {
            $this->logger->info($error);
        }
    }

    protected function determineStatusCode(): int
    {
        foreach (self::CODE_WHITELIST as $item) {
            if (is_a($this->exception, $item)) {
                return $this->exception->getCode();
            }
        }

        return parent::determineStatusCode();
    }
}
