<?php

declare(strict_types=1);

namespace Laudis\UserManagement;

use Base64Url\Base64Url;
use DI\ContainerBuilder;
use DI\Definition\Source\SourceCache;
use Exception;
use Laudis\Common\Databags\AppConfig;
use Laudis\UserManagement\Controllers\AuthTokenIssueController;
use Laudis\UserManagement\Controllers\ChangePasswordWithLinkController;
use Laudis\UserManagement\Controllers\ResetPasswordLinkController;
use Laudis\UserManagement\Controllers\RouteListController;
use Laudis\UserManagement\Controllers\UpdatePasswordController;
use Laudis\UserManagement\Controllers\UserController;
use Laudis\UserManagement\Middleware\AddPossibleUserMiddleware;
use Laudis\UserManagement\Middleware\AuthenticateMiddleware;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Factory\AppFactory;
use Slim\Interfaces\MiddlewareDispatcherInterface;
use Slim\Interfaces\RouteCollectorProxyInterface;

use SodiumException;
use function apcu_clear_cache;
use function array_map;
use function array_walk_recursive;
use function exec;
use function getenv;
use function scandir;
use function sodium_crypto_sign_keypair;
use function sodium_crypto_sign_publickey;
use function sodium_crypto_sign_secretkey;

/** @noinspection PhpUnusedParameterInspection */
function reset_password_email(string $url): string
{
    return require __DIR__ . '/../resources/mails/reset_password.php';
}

function add_to_router(RouteCollectorProxyInterface $proxy): void
{
    $proxy->post('/token', AuthTokenIssueController::class)->setName('issueToken')->add('token_validator');
    $proxy->post('/reset-password', ResetPasswordLinkController::class)->setName('resetPasswordLink')
        ->add('reset_password_link_validator');
    $proxy->post('/change-password/{token}', ChangePasswordWithLinkController::class)->setName('changePasswordWithLink')
        ->add('change_password_with_link_validator');
    $proxy->post('/update-password', UpdatePasswordController::class)->setName('updatePassword')
        ->add('update_password_validator')
        ->add(AuthenticateMiddleware::class);
    $proxy->get('/routes', RouteListController::class)->setName('routes');

    $proxy->get('/user', UserController::class . ':presentUser')->setName('presentUser')
        ->add(AuthenticateMiddleware::class);
    $proxy->get('/intro-message', UserController::class . ':introMessage')->setName('introMessage')
        ->add(AuthenticateMiddleware::class);
}

function add_global_middleware(MiddlewareDispatcherInterface $dispatcher): void
{
    $dispatcher->add(AddPossibleUserMiddleware::class);
}

/**
 * @throws SodiumException
 */
function generate_public_private_key(): array
{
    $pair = sodium_crypto_sign_keypair();
    $public = sodium_crypto_sign_publickey($pair);
    $private = sodium_crypto_sign_secretkey($pair);

    $secretLength = mb_strlen($private, '8bit');
    $private = mb_substr($private, 0, -$secretLength / 2, '8bit');

    $public = Base64Url::encode($public);
    $private = Base64Url::encode($private);

    return [$public, $private];
}


/**
 * @throws Exception
 */
function make_container(?array $definitions = null): ContainerInterface
{
    $builder = new ContainerBuilder();

    $builder->addDefinitions(__DIR__ . '/../vendor/laudis/common/src/register.php');
    $builder->addDefinitions(__DIR__ . '/register.php');

    if ($definitions) {
        $builder->addDefinitions($definitions);
    }

    $builder->useAnnotations(false);
    $builder->useAutowiring(false);

    if (getenv('APP_ENV') === 'production') {
        $builder->enableCompilation(__DIR__ . '/../temp/di');
        $builder->enableDefinitionCache();
    }

    return $builder->build();
}

/**
 * @throws Exception
 */
function boot_app(array $definitions = null): App
{
    $container = make_container($definitions);
    $app = AppFactory::createFromContainer($container);
    $middleware = $app->addErrorMiddleware(true, true, true);
    $handler = new ErrorHandler(
        $app->getCallableResolver(),
        $app->getResponseFactory(),
        $container->get(LoggerInterface::class)
    );
    $errorRenderer = new CustomJsonErrorRenderer();
    $handler->registerErrorRenderer('application/json', $errorRenderer);
    $handler->setDefaultErrorRenderer('application/json', $errorRenderer);
    $handler->forceContentType('application/json');
    $middleware->setDefaultErrorHandler($handler);
    $app->addRoutingMiddleware();
    $config = $container->get(AppConfig::class);
    if ($config->getEnv() === 'production') {
        $app->getRouteCollector()->setCacheFile(__DIR__ . '/../temp/slim/routes.cache');
    }
    add_to_router($app);
    return $app;
}


/** @noinspection PhpUndefinedVariableInspection */
function clear_cache(array $dirs = null): void
{
    $dir = __DIR__ . '/../temp';
    $scandir = scandir("$dir/$map", SCANDIR_SORT_ASCENDING);
    $dirs ??= array_map(
        static fn(string $map) => array_map(fn(string $file) => "$dir/$map/$file", $scandir),
        ['di', 'htmlpurifier', 'mpdf', 'phpunit', 'slim']
    );

    array_walk_recursive(
        $dirs,
        static function (string $folder) {
            if (mb_substr($folder, -mb_strlen('.')) !== '.' &&
                mb_substr($folder, -mb_strlen('..')) !== '..' &&
                mb_substr($folder, -mb_strlen('.gitignore')) !== '.gitignore') {
                exec('rm -Rf ' . $folder);
            }
        }
    );

    if (SourceCache::isSupported()) {
        apcu_clear_cache();
    }
}
