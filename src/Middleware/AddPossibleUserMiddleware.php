<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Middleware;

use Exception;
use Laudis\UserManagement\JWS\JWSLoader;
use Laudis\UserManagement\UserManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

use function array_intersect_key;
use function count;

final class AddPossibleUserMiddleware implements MiddlewareInterface
{
    private const AUTHORIZATION_KEYS = [
        'user_id',
        'email',
        'username',
        'roles',
        'created_at',
        'updated_at',
        'guest_login',
        'type'
    ];
    private const LAUDIS_KEYS = [
        'user_id',
        'type'
    ];

    private UserManager $manager;
    private JWSLoader $loader;

    public function __construct(UserManager $manager, JWSLoader $loader)
    {
        $this->manager = $manager;
        $this->loader = $loader;
    }

    /**
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $authToken = $this->getToken($request, 'Authorization');
        $laudisToken = $this->getToken($request, 'Authentication');

        $this->login($authToken, $laudisToken);

        return $handler->handle($request);
    }

    /**
     * @throws Exception
     */
    private function login(string $authToken, string $laudisToken): void
    {
        try {
            $payloadAuthToken = $this->loader->load($authToken);
            $payloadLaudisToken = $this->loader->load($laudisToken);
        } catch (Exception $e) {
            return;
        }

        /** @noinspection NotOptimalIfConditionsInspection false positive. */
        if (
            $this->containsAllKeys($payloadAuthToken, self::AUTHORIZATION_KEYS) &&
            $this->containsAllKeys($payloadLaudisToken, self::LAUDIS_KEYS) &&
            $payloadAuthToken['type'] === 'auth' &&
            $payloadAuthToken['type'] === 'laudis-authentication'
        ) {
            return;
        }

        $this->manager->login($payloadAuthToken);
    }


    public function getToken(ServerRequestInterface $request, string $tokenName): string
    {
        $token = $request->getHeaderLine($tokenName);
        if ($token === '') {
            $token = $request->getCookieParams()[$tokenName] ?? '';
        }
        return $token;
    }

    private function containsAllKeys(array $payload, array $keys): bool
    {
        return count(array_intersect_key($payload, array_flip($keys))) === count($keys);
    }
}
