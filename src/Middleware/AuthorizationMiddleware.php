<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Middleware;

use Casbin\Enforcer;
use Casbin\Exceptions\CasbinException;
use JsonException;
use Laudis\UserManagement\Exceptions\UnauthorizedException;
use Laudis\UserManagement\UserManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class AuthorizationMiddleware implements MiddlewareInterface
{
    private Enforcer $enforcer;
    private UserManager $manager;

    public function __construct(Enforcer $enforcer, UserManager $manager)
    {
        $this->manager = $manager;
        $this->enforcer = $enforcer;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws CasbinException
     * @throws JsonException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        [$user, $userString] = $this->presentUser();
        $method = $request->getMethod();
        $path = $request->getUri()->getPath();
        $data = $this->presentData($request);
        /** @noinspection PhpPossiblePolymorphicInvocationInspection */
        if ($user && $this->enforcer->getAdapter()->isFiltered()) {
            $this->enforcer->loadFilteredPolicy($user);
        }  else {
            $this->enforcer->loadPolicy();
        }
        if (!$this->enforcer->enforce($userString, $method, $path, $data)) {
            throw new UnauthorizedException("$userString has no permission to $method $path on $data");
        }

        return $handler->handle($request);
    }

    private function presentUser(): array
    {
        $user = $this->manager->getLoggedInUser();
        return [
            $user,
            $user === null ? 'null' : ('user' . $user->getId())
        ];
    }

    /**
     * @throws JsonException
     */
    private function presentData(ServerRequestInterface $request): string
    {
        $data = $request->getMethod() === 'GET' ? $request->getQueryParams() : $request->getParsedBody();
        return json_encode($data, JSON_THROW_ON_ERROR, 1);
    }
}
