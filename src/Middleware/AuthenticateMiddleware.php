<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Middleware;

use DateTime;
use Exception;
use Laudis\UserManagement\Databags\AppConfig;
use Laudis\UserManagement\Databags\GuestLogin;
use Laudis\UserManagement\Exceptions\UnauthenticatedException;
use Laudis\UserManagement\UserManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class AuthenticateMiddleware implements MiddlewareInterface
{
    private UserManager $manager;
    private AppConfig $config;

    public function __construct(UserManager $manager, AppConfig $config)
    {
        $this->manager = $manager;
        $this->config = $config;
    }

    /**
     * @throws UnauthenticatedException
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->config->isAuthOn()) {
            $user = $this->manager->getLoggedInUser();
            if ($user === null) {
                throw new UnauthenticatedException('User is not authenticated');
            }
            $guestLogin = $this->manager->getGuestLogin();
            if ($guestLogin !== null && $this->expiredLogin($guestLogin)) {
                throw new UnauthenticatedException('Uw account is verlopen');
            }
        }
        return $handler->handle($request);
    }

    private function expiredLogin(GuestLogin $guestLogin): bool
    {
        return ($guestLogin->getValidUntil())->getTimestamp() < (new DateTime())->getTimestamp();
    }
}
