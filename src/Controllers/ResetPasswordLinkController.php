<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Controllers;

use DateTime;
use Exception;
use Laudis\Common\Contracts\Psr7Helper;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\JWS\JWSFactory;
use Laudis\UserManagement\ResetMailSender;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use UnexpectedValueException;

/**
 * \class ResetPasswordController
 * controller dealing with all requests about resetting a password.
 */
final class ResetPasswordLinkController
{
    private Psr7Helper $writer;
    private JWSFactory $factory;
    private UserRepositoryInterface $repository;
    private ResetMailSender $sender;

    public function __construct(
        Psr7Helper $writer,
        JWSFactory $factory,
        ResetMailSender $sender,
        UserRepositoryInterface $repository
    ) {
        $this->writer = $writer;
        $this->factory = $factory;
        $this->repository = $repository;
        $this->sender = $sender;
    }

    /**
     * @throws Exception
     * @throws ClientExceptionInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $email = $this->writer->parseAsArray($request)['email'];

        $user = $this->repository->findUser($email);
        if ($user === null) {
            throw new UnexpectedValueException('Could not find user with email: ' . $email);
        }

        $token = $this->factory->make(
            $user,
            null,
            [
                'email' => $email,
                'exp' => (new DateTime('+1 day'))->getTimestamp(),
                'type' => 'reset',
            ]
        );

        $this->repository->addPasswordReset($user, $token);

        $this->sender->send($email, $token);

        return $this->writer->write($response, ['message' => 'De link werd succesvol verzonden.']);
    }
}
