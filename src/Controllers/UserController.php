<?php

/** @noinspection PhpUnusedParameterInspection */

declare(strict_types=1);

namespace Laudis\UserManagement\Controllers;

use InvalidArgumentException;
use JsonException;
use Laudis\Common\Contracts\Psr7Helper;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Exceptions\UnauthenticatedException;
use Laudis\UserManagement\UserManager;
use Laudis\UserManagement\UserPresenter;
use PDOException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;

final class UserController
{
    private UserManager $manager;
    private UserPresenter $presenter;
    private UserRepositoryInterface $repository;
    private Psr7Helper $helper;

    public function __construct(
        UserManager $manager,
        UserPresenter $presenter,
        Psr7Helper $helper,
        UserRepositoryInterface $repository
    ) {
        $this->manager = $manager;
        $this->presenter = $presenter;
        $this->helper = $helper;
        $this->repository = $repository;
    }

    /**
     * @throws PDOException
     * @throws UnauthenticatedException
     * @throws InvalidArgumentException
     * @throws JsonException
     * @throws RuntimeException
     */
    public function presentUser(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $user = $this->manager->getLoggedInUserStrict();
        return $this->helper->write($response, $this->presenter->present($user));
    }

    /**
     * @throws InvalidArgumentException
     * @throws JsonException
     * @throws PDOException
     * @throws RuntimeException
     * @throws UnauthenticatedException
     */
    public function introMessage(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $user = $this->manager->getLoggedInUserStrict();
        $value = $request->getParsedBody()['userSeenMessage'] ?? true;

        $this->repository->setUserSeenMessage($user, $value);

        return $this->helper->write($response, ['message' => 'succcess']);
    }
}
