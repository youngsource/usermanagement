<?php
declare(strict_types=1);


namespace Laudis\UserManagement\Controllers;


use InvalidArgumentException;
use JsonException;
use Laudis\Common\Contracts\Psr7Helper;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use Slim\Interfaces\RouteCollectorInterface;
use Slim\Interfaces\RouteInterface;

final class RouteListController
{
    private Psr7Helper $helper;
    private RouteCollectorInterface $collector;

    public function __construct(Psr7Helper $helper, RouteCollectorInterface $collector)
    {
        $this->helper = $helper;
        $this->collector = $collector;
    }

    /**
     * @throws InvalidArgumentException
     * @throws JsonException
     * @throws RuntimeException
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        $routes = array_map(static function (RouteInterface $route) {
            return [
                'name' => $route->getName(),
                'methods' => $route->getMethods(),
                'arguments' => $route->getArguments(),
                'identifier' => $route->getIdentifier(),
                'pattern' => $route->getPattern()
            ];
         }, $this->collector->getRoutes());

        return $this->helper->write($response, array_values($routes));
    }
}