<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Controllers;

use Exception;
use Laudis\Common\Contracts\Psr7Helper;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\JWS\JWSLoader;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use UnexpectedValueException;

/**
 * Class ChangePasswordWithLinkController.
 */
final class ChangePasswordWithLinkController
{
    private PasswordHasherInterface $hasher;
    private UserRepositoryInterface $repository;
    private Psr7Helper $responseWriter;
    private JWSLoader $loader;

    public function __construct(
        PasswordHasherInterface $hasher,
        UserRepositoryInterface $repository,
        JWSLoader $loader,
        Psr7Helper $responseWriter
    ) {
        $this->hasher = $hasher;
        $this->repository = $repository;
        $this->responseWriter = $responseWriter;
        $this->loader = $loader;
    }

    /**
     * @throws Exception
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $result = $this->responseWriter->parseAsArray($request);

        $password = $result['password'];
        $token = $result['token'];

        $token = $this->loader->load($token);

        $user = $this->repository->findUser($token['email']);
        if ($user === null) {
            throw new UnexpectedValueException('De gebruiker werd niet gevonden');
        }

        $user->setPasswordHash($this->hasher->hash($password));

        $this->repository->updateUser($user);
        $this->repository->usePasswordResets($user);

        return $this->responseWriter->write($response, ['message' => 'Het wachtwoord werd succesvol geupdate.']);
    }
}
