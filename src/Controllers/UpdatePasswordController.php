<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Controllers;

use InvalidArgumentException;
use JsonException;
use Laudis\Common\Contracts\Psr7Helper;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Exceptions\UnauthenticatedException;
use Laudis\UserManagement\UserManager;
use PDOException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use UnexpectedValueException;

/**
 * Class UpdateUserController.
 */
final class UpdatePasswordController
{
    private UserRepositoryInterface $repository;
    private Psr7Helper $helper;
    private PasswordHasherInterface $hasher;
    private UserManager $manager;

    public function __construct(
        UserRepositoryInterface $repository,
        Psr7Helper $helper,
        PasswordHasherInterface $hasher,
        UserManager $manager
    ) {
        $this->repository = $repository;
        $this->helper = $helper;
        $this->hasher = $hasher;
        $this->manager = $manager;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws PDOException
     * @throws UnauthenticatedException
     * @throws InvalidArgumentException
     * @throws JsonException
     * @throws RuntimeException
     * @throws UnexpectedValueException
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $body = $this->helper->parseAsArray($request);

        $user = $this->manager->getLoggedInUserStrict();
        $user->setPasswordHash($this->hasher->hash($body['password']));
        $this->repository->updateUser($user);

        return $this->helper->write($response, ['message' => 'Het wachtwoord werd succesvol aangepast']);
    }
}
