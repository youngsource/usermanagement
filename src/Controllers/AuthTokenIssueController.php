<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Controllers;

use Exception;
use Laudis\Common\Contracts\Psr7Helper;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Databags\User;
use Laudis\UserManagement\JWS\JWSFactory;
use Laudis\UserManagement\UserPresenter;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AuthTokenIssueController
{
    private Psr7Helper $helper;
    private UserRepositoryInterface $repository;
    private JWSFactory $factory;
    private UserPresenter $presenter;

    public function __construct(
        Psr7Helper $helper,
        UserRepositoryInterface $repository,
        JWSFactory $factory,
        UserPresenter $presenter
    ) {
        $this->helper = $helper;
        $this->repository = $repository;
        $this->factory = $factory;
        $this->presenter = $presenter;
    }

    /**
     * @throws Exception
     */
    public function __invoke(Request $request, ResponseInterface $response): ResponseInterface
    {
        $body = $this->helper->parseAsArray($request);
        $user = $this->cast($this->repository->findUser($body['email']));

        $payload = array_merge($this->presenter->presentForToken($user), ['type' => 'auth']);
        $token = $this->factory->make($user, $body['aud'], $payload);
        return $this->helper->write($response, [
            'token' => $token,
            'authenticationToken' => $this->factory->make($user, null, $this->presenter->presentForId($user))
        ]);
    }

    /**
     * @suppress PhanTypeMismatchReturnNullable
     */
    private function cast($user): User
    {
        return $user;
    }
}
