<?php

declare(strict_types=1);

namespace Laudis\UserManagement;

use Exception;
use Laudis\UserManagement\Databags\GuestLogin;
use Laudis\UserManagement\Databags\User;
use Laudis\UserManagement\Exceptions\UnauthenticatedException;

final class UserManager
{
    private ?User $user = null;
    private array $roles = [];
    private ?GuestLogin $guestLogin = null;
    private UserFactory $factory;
    private ?string $aud;

    public function __construct(UserFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return string|null
     */
    public function getAud(): ?string
    {
        return $this->aud;
    }

    public function getLoggedInUser(): ?User
    {
        return $this->user;
    }

    /**
     * Gets the user or throws an authenticationexception if it does not have one.
     *
     * @throws UnauthenticatedException
     */
    public function getLoggedInUserStrict(): User
    {
        if ($this->user === null) {
            throw new UnauthenticatedException();
        }
        return $this->user;
    }

    /**
     * @throws UnauthenticatedException
     */
    public function getRoles(): array
    {
        if ($this->user === null) {
            throw new UnauthenticatedException();
        }
        return $this->roles;
    }

    /**
     * @throws UnauthenticatedException
     */
    public function getGuestLogin(): ?GuestLogin
    {
        if ($this->user === null) {
            throw new UnauthenticatedException();
        }
        return $this->guestLogin;
    }

    /**
     * @throws Exception
     * @suppress PhanPartialTypeMismatchProperty
     */
    public function login(array $token): void
    {
        $token['id'] = $token['user_id'];
        $this->user = $this->factory->userFromAssociativeArray($token);
        $this->roles = array_map([$this->factory, 'roleFromAssociativeArray'], $token['roles']);
        $this->guestLogin = $token['guest_login'] === null ? null : $this->factory->guestLoginFromAssociativeArray(
            $token['guest_login']
        );
        $this->aud = $token['aud'];
    }
}
