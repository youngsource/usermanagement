<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Validation;

use Exception;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\JWS\JWSLoader;
use Psr\Log\LoggerInterface;
use Rakit\Validation\Rule;

use function time;


final class ResetTokenRule extends Rule
{
    private JWSLoader $loader;
    private UserRepositoryInterface $repository;
    private LoggerInterface $logger;

    public function __construct(JWSLoader $loader, UserRepositoryInterface $repository, LoggerInterface $logger)
    {
        $this->setMessage('De reset link is verlopen');
        $this->loader = $loader;
        $this->repository = $repository;
        $this->logger = $logger;
    }

    /**
     * @param mixed $value
     * @return bool
     * @throws Exception
     * @suppress PhanUnusedVariableCaughtException
     */
    public function check($value): bool
    {
        $token = (string)$value;

        try {
            $payload = $this->loader->load($token);
        } catch (Exception $e) {
            $this->logger->info($e->getMessage(), $e->getTrace());
            $this->setMessage('Dit is geen geldige resetToken');

            return false;
        }

        if (($payload['type'] ?? '') !== 'reset' || !isset($payload['email'])) {
            $this->setMessage('Dit is geen reset token.');

            return false;
        }

        $user = $this->repository->findUser($payload['email']);
        if ($user === null) {
            $this->setMessage('Deze gebruiker bestaat niet meer.');

            return false;
        }

        foreach ($this->repository->getPasswordResets($user) as $reset) {
            if ($reset->getToken() === $token) {
                if ($reset->isUsed()) {
                    $this->setMessage('Deze reset token is al gebruikt');
                    return false;
                }
                return $payload['exp'] > time();
            }
        }

        return false;
    }
}
