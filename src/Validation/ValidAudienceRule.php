<?php
declare(strict_types=1);


namespace Laudis\UserManagement\Validation;


use Laudis\UserManagement\Databags\AppConfig;
use Rakit\Validation\Rule;

use function array_intersect;
use function count;

final class ValidAudienceRule extends Rule
{
    private AppConfig $config;

    public function __construct(AppConfig $config)
    {
        $this->config = $config;
    }

    public function check($value): bool
    {
        $audienceIntersection = array_intersect($this->config->getAudiences(), [$value]);
        return count($audienceIntersection) !== 0;
    }
}