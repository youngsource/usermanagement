<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Validation;

use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Databags\User;
use Laudis\UserManagement\UserManager;
use PDOException;
use Rakit\Validation\Rule;


final class PasswordRule extends Rule
{
    private PasswordHasherInterface $hasher;
    private UserManager $manager;
    private UserRepositoryInterface $repository;

    public function __construct(
        PasswordHasherInterface $hasher,
        UserManager $manager,
        UserRepositoryInterface $repository
    ) {
        $this->hasher = $hasher;
        $this->setMessage('Het wachtwoord is niet correct.');
        $this->manager = $manager;
        $this->repository = $repository;
    }

    /**
     * @throws PDOException
     */
    public function check($value): bool
    {
        // Only validatate if the validation is passing, otherwise this might give away too much info to the client.
        if ($this->validation->passes()) {
            $email = $this->validation->getValue('email');
            if ($email === null) {
                $user = $this->manager->getLoggedInUser();
                if ($user === null) {
                    $this->setMessage('We cannot validate a password without a valid user');
                    return false;
                }
                $email = $user->getEmail();
            }

            $user = $this->repository->findUser($email);
            if ($user === null) {
                return false;
            }
            $result = $this->hasher->check($value, $user->getPasswordHash());

            if ($result === true) {
                $guest = $this->repository->getGuestLogin($user);
                if ($guest !== null && $guest->getValidUntil()->getTimestamp() < time()) {
                    $this->setMessage('Uw login is vervallen, gelieve te verlengen');
                    return false;
                }
            }

            return $result;
        }

        return true;
    }
}
