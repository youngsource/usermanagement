<?php
declare(strict_types=1);

namespace Laudis\UserManagement\JWS;


use Exception;
use Jose\Component\Checker\ClaimCheckerManager;
use Jose\Component\Core\JWK;
use Jose\Component\Core\JWKSet;
use Jose\Component\Signature\JWSLoader as Loader;

use function array_map;

final class JWSLoader
{
    private Loader $loader;
    private JWKSet $keys;
    private ClaimCheckerManager $manager;

    public function __construct(Loader $loader, JWKSet $keys, ClaimCheckerManager $manager)
    {
        $this->loader = $loader;
        $this->keys = $keys;
        $this->manager = $manager;
    }

    /**
     * @throws Exception
     */
    public function load(string $token): array
    {
        $keys = $this->keys->all();
        array_map(static fn(JWK $key) => $key->toPublic(), $keys);
        $keyset = new JWKSet($keys);

        $jws = $this->loader->loadAndVerifyWithKeySet($token, $keyset, $signature);
        $payload = safe_json_decode($jws->getPayload() ?? '');
        /** @noinspection UnusedFunctionResultInspection */
        $this->manager->check($payload);
        return $payload;
    }
}
