<?php

declare(strict_types=1);

namespace Laudis\UserManagement\JWS;

use DateTime;
use Exception;
use InvalidArgumentException;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Jose\Component\Signature\Serializer\JWSSerializerManager;
use Laudis\UserManagement\Databags\AppConfig;
use Laudis\UserManagement\Databags\User;

use function array_merge;
use function time;
use function uuid;

use const JSON_PRETTY_PRINT;

final class JWSFactory
{
    private JWSSerializerManager $serializer;
    private AppConfig $config;
    private JWSBuilder $builder;

    public function __construct(JWSBuilder $builder, JWSSerializerManager $serializer, AppConfig $config)
    {
        $this->builder = $builder;
        $this->config = $config;
        $this->serializer = $serializer;
    }

    /**
     * @throws Exception
     */
    private function basicTokenPayload(User $user, array $payload): string
    {
        return safe_json_encode(
            array_merge(
                [
                    'iat' => time(),
                    'nbf' => time(),
                    'exp' => (new DateTime('+1 hour'))->getTimestamp(),
                    'iss' => $this->config->getName(),
                    'aud' => $this->config->getAudience(),
                    'user_id' => $user->getId(),
                    'jti' => uuid()
                ],
                $payload
            ),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * @throws Exception
     */
    public function make(User $user, ?string $aud = null, array $payload = ['type' => 'auth']): string
    {
        if (!isset($payload['type'])) {
            throw new InvalidArgumentException('Payload must contain a type');
        }
        $toMerge = [];
        if ($aud !== null) {
            $toMerge = ['aud' => $aud];
        }
        $jws = $this->builder->withPayload(
            $this->basicTokenPayload($user, array_merge($payload, $toMerge))
        )->build();
        return $this->serializer->serialize(CompactSerializer::NAME, $jws, 0);
    }
}
