<?php
declare(strict_types=1);


namespace Laudis\UserManagement\Contracts;


use Laudis\UserManagement\Databags\Role;
use Laudis\UserManagement\Databags\User;
use PDOException;

interface RolesRepositoryInterface
{
    /**
     * @return Role[]
     * @throws PDOException
     */
    public function getRolesForUser(User $user): array;

    /**
     * @return Role[]
     * @throws PDOException
     */
    public function getAllRoles(int $limit = 200, int $offset = 0): array;

    /**
     * @throws PDOException
     */
    public function getRole(string $role): ?Role;

    /**
     * @throws PDOException
     */
    public function deleteRole(Role $role): void;

    /**
     * @throws PDOException
     */
    public function syncRole(User $user, Role $role): void;

    /**
     * @throws PDOException
     */
    public function detachRole(User $user, Role $role): void;

    /**
     * @throws PDOException
     */
    public function insertRole(string $role): Role;
}
