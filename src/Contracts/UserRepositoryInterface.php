<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Contracts;

use DateTimeInterface;
use Laudis\UserManagement\Databags\GuestLogin;
use Laudis\UserManagement\Databags\PasswordReset;
use Laudis\UserManagement\Databags\User;
use PDOException;

interface UserRepositoryInterface
{
    /**
     * @throws PDOException
     */
    public function insertUser(string $email, string $username, string $passwordHash): void;

    /**
     * @throws PDOException
     */
    public function findUser(string $email): ?User;

    /**
     * @throws PDOException
     */
    public function getUserById(int $id): ?User;

    /**
     * @throws PDOException
     */
    public function updateUser(User $user): void;

    /**
     * @throws PDOException
     */
    public function deleteUser(User $user): void;

    /**
     * @return User[]
     * @throws PDOException
     */
    public function getAllUsers(int $limit = 200, int $start = 0): array;

    /**
     * @throws PDOException
     */
    public function usePasswordResets(User $user): void;

    /**
     * @throws PDOException
     */
    public function getGuestLogin(User $user): ?GuestLogin;

    /**
     * @throws PDOException
     */
    public function addPasswordReset(User $user, string $token): void;

    /**
     * @throws PDOException
     */
    public function setUserSeenMessage(User $user, bool $seen): void;

    /**
     * @return PasswordReset[]
     * @throws PDOException
     */
    public function getPasswordResets(User $user): array;

    /**
     * @throws PDOException
     */
    public function makeOrUpdateGuestLogin(User $user, DateTimeInterface $validUntil): void;
}
