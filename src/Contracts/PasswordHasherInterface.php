<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Contracts;

/**
 * Interface PasswordHasherInterface.
 * @psalm-immutable
 */
interface PasswordHasherInterface
{
    /**
     * Hashes the password.
     */
    public function hash(string $password): string;

    /**
     * Checks the given password against the given hash.
     */
    public function check(string $password, string $hash): bool;
}
