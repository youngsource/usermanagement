<?php
declare(strict_types=1);


namespace Laudis\UserManagement;


use BadMethodCallException;
use Casbin\Model\Model;
use Casbin\Persist\FilteredAdapter;
use InvalidArgumentException;
use Laudis\UserManagement\Contracts\RolesRepositoryInterface;
use Laudis\UserManagement\Databags\Role;
use Laudis\UserManagement\Databags\User;
use PDO;
use PDOException;
use function count;

final class RequestPoliciesAdapter implements FilteredAdapter
{
    private PDO $pdo;
    private bool $filtered = true;
    private RolesRepositoryInterface $repository;

    public function __construct(PDO $pdo, RolesRepositoryInterface $repository)
    {
        $this->pdo = $pdo;
        $this->repository = $repository;
    }

    public function loadPolicy(Model $model): void
    {
        // There aren't any policies to load without a filter
    }

    /**
     * @throws BadMethodCallException
     */
    public function savePolicy(Model $model): void
    {
        throw new BadMethodCallException(static::class . '::savePolicy is unsupported');
    }

    /**
     * @throws BadMethodCallException
     */
    public function addPolicy(string $sec, string $ptype, array $rule): void
    {
        throw new BadMethodCallException(static::class . '::addPolicy is unsupported');
    }

    /**
     * @throws BadMethodCallException
     */
    public function removePolicy(string $sec, string $ptype, array $rule): void
    {
        throw new BadMethodCallException(static::class . '::removePolicy is unsupported');
    }

    /**
     * @throws BadMethodCallException
     */
    public function removeFilteredPolicy(string $sec, string $ptype, int $fieldIndex, string ...$fieldValues): void
    {
        throw new BadMethodCallException(static::class . '::removeFilteredPolicy is unsupported');
    }

    /**
     * @throws InvalidArgumentException
     * @throws PDOException
     */
    public function loadFilteredPolicy(Model $model, $filter): void
    {
        if (!$filter instanceof User) {
            throw new InvalidArgumentException('The filter must be a ' . User::class);
        }

        $roles = array_map(static fn(Role $role) => $role->getId(), $this->repository->getRolesForUser($filter));
        $rolesCount = count($roles);
        if ($rolesCount) {
            $placeholders = str_repeat('?,', $rolesCount - 1) . '?';
            $policies = $this->pdo->prepare("
SELECT role_id, `act`, path, `data` FROM request_policies WHERE role_id IN ($placeholders)
");
        } else {
            $policies = null;
        }
        if ($policies && $policies->execute($roles)) {
            foreach ($policies->fetchAll() as $policy) {
                $model['p']['p']->policy[] = array_values(array_map('strval', $policy));
            }
        }
        $userId = $filter->getId();
        foreach ($roles as $role) {
            $model['g']['g']->policy[] = ['user' . $userId, (string)$role];
        }
    }

    public function isFiltered(): bool
    {
        return $this->filtered;
    }
}