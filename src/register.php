<?php

declare(strict_types=1);

use Casbin\Enforcer;
use Casbin\Log\Log;
use Casbin\Log\Logger;
use Casbin\Persist\FilteredAdapter;
use FastRoute\Dispatcher\GroupCountBased;
use FastRoute\RouteCollector;
use Jose\Component\Checker\AlgorithmChecker;
use Jose\Component\Checker\ClaimCheckerManager;
use Jose\Component\Checker\ExpirationTimeChecker;
use Jose\Component\Checker\HeaderCheckerManager;
use Jose\Component\Checker\IssuedAtChecker;
use Jose\Component\Checker\NotBeforeChecker;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\JWK;
use Jose\Component\Core\JWKSet;
use Jose\Component\Signature\Algorithm\EdDSA;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\JWSTokenSupport;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Jose\Component\Signature\Serializer\JWSSerializerManager;
use Laudis\Common\Contracts\Psr7Helper;
use Laudis\Common\JsonPsr7Helper;
use Laudis\Common\MailSender;
use Laudis\Common\Middleware\ValidationMiddleware;
use Laudis\UserManagement\CasbinPsrLogger;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\RolesRepositoryInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Controllers\AuthTokenIssueController;
use Laudis\UserManagement\Controllers\ChangePasswordWithLinkController;
use Laudis\UserManagement\Controllers\ResetPasswordLinkController;
use Laudis\UserManagement\Controllers\RouteListController;
use Laudis\UserManagement\Controllers\UpdatePasswordController;
use Laudis\UserManagement\Controllers\UserController;
use Laudis\UserManagement\Databags\AppConfig;
use Laudis\UserManagement\JWS\JWSFactory;
use Laudis\UserManagement\JWS\JWSLoader;
use Laudis\UserManagement\Middleware\AddPossibleUserMiddleware;
use Laudis\UserManagement\Middleware\AuthenticateMiddleware;
use Laudis\UserManagement\Middleware\AuthorizationMiddleware;
use Laudis\UserManagement\MultipleAudienceChecker;
use Laudis\UserManagement\PasswordHasher;
use Laudis\UserManagement\Repositories\RolesRepository;
use Laudis\UserManagement\Repositories\UserRepository;
use Laudis\UserManagement\RequestPoliciesAdapter;
use Laudis\UserManagement\ResetMailSender;
use Laudis\UserManagement\UserFactory;
use Laudis\UserManagement\UserManager;
use Laudis\UserManagement\UserPresenter;
use Laudis\UserManagement\Validation\PasswordRule;
use Laudis\UserManagement\Validation\ResetTokenRule;
use Laudis\UserManagement\Validation\ValidAudienceRule;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Rakit\Validation\Validator;
use Slim\App;
use function DI\decorate;
use function FastRoute\simpleDispatcher;

return [
    UserRepositoryInterface::class => static function (ContainerInterface $container) {
        return new UserRepository(
            $container->get(PDO::class),
            new UserFactory()
        );
    },

    RouteListController::class => static function (ContainerInterface $container) {
        return new RouteListController(
            $container->get(Psr7Helper::class),
            $container->get(App::class)->getRouteCollector()
        );
    },

    PasswordHasherInterface::class => static function () {
        return new PasswordHasher();
    },

    AuthTokenIssueController::class => static function (ContainerInterface $container) {
        return new AuthTokenIssueController(
            $container->get(Psr7Helper::class),
            $container->get(UserRepositoryInterface::class),
            $container->get(JWSFactory::class),
            $container->get(UserPresenter::class)
        );
    },

    'token_validator' => static function (ContainerInterface $container) {
        return new ValidationMiddleware(
            $container->get(Validator::class)->make(
                [],
                [
                    'email' => 'required|exists:users,email',
                    'password' => 'required|password',
                    'aud' => 'required|valid-audience',
                ]
            ),
            $container->get(Psr7Helper::class)
        );
    },

    UserController::class => static function (ContainerInterface $container) {
        return new UserController(
            $container->get(UserManager::class),
            $container->get(UserPresenter::class),
            $container->get(Psr7Helper::class),
            $container->get(UserRepositoryInterface::class)
        );
    },

    AddPossibleUserMiddleware::class => static function (ContainerInterface $container) {
        return new AddPossibleUserMiddleware(
            $container->get(UserManager::class),
            $container->get(JWSLoader::class)
        );
    },

    AppConfig::class => static function (ContainerInterface $container) {
        return new AppConfig(
            'gallop',
            '7WZj2sPvE58HkKM1YlXLA0wqfjF2gNDvjfqt0CkMtt8',
            'R85hn6VHKk_zsy3mbbY_bJ8tkROB6cALth-uUtL6NGI',
            true,
            'R85hn6VHKk_zsy3mbbY_bJ8tkROB6cALth-uUtL6NGI',
            'local',
            explode(',', 'development,connect,practinet,calculators,gallopp'),
            'connect',
            'localhost.com'
        );
    },

    UserManager::class => static function (ContainerInterface $container) {
        return new UserManager($container->get(UserFactory::class));
    },

    AuthenticateMiddleware::class => static function (ContainerInterface $container) {
        return new AuthenticateMiddleware($container->get(UserManager::class), $container->get(AppConfig::class));
    },

    Enforcer::class => static function (ContainerInterface $container) {
        $enforcer = new Enforcer(__DIR__ . '/../resources/config/model.conf', $container->get(FilteredAdapter::class));
        $enforcer->addFunction('keyMatch4', $container->get('keyMatch4'));
        $config = $container->get(AppConfig::class);
        Log::setLogger($container->get(Logger::class));
        if (!$config->isAuthOn()) {
            $enforcer->enableEnforce(false);
        }
        $enforcer->enableLog(true);
        return $enforcer;
    },

    'keyMatch4' => static function () {
        return static function (string $x, string $y) {
            $dispatcher = simpleDispatcher(
                static function (RouteCollector $r) use ($y) {
                    $r->addRoute('GET', $y, 'get_all_users_handler');
                }
            );
            $result = $dispatcher->dispatch('GET', $x);
            return $result[0] !== GroupCountBased::NOT_FOUND;
        };
    },

    FilteredAdapter::class => static function (ContainerInterface $container) {
        return new RequestPoliciesAdapter(
            $container->get(PDO::class), $container->get(RolesRepositoryInterface::class));
    },

    Logger::class => static function (ContainerInterface $container) {
        return new CasbinPsrLogger($container->get(LoggerInterface::class));
    },

    Validator::class => decorate(
        static function (Validator $validator, ContainerInterface $container) {
            $validator->addValidator('password', $container->get(PasswordRule::class));
            $validator->addValidator('reset-token', $container->get(ResetTokenRule::class));
            $validator->addValidator('valid-audience', $container->get(ValidAudienceRule::class));
            return $validator;
        }
    ),

    ValidAudienceRule::class => static function (ContainerInterface $container) {
        return new ValidAudienceRule($container->get(AppConfig::class));
    },

    ResetTokenRule::class => static function (ContainerInterface $container) {
        return new ResetTokenRule(
            $container->get(JWSLoader::class),
            $container->get(UserRepositoryInterface::class),
            $container->get(LoggerInterface::class)
        );
    },

    ChangePasswordWithLinkController::class => static function (ContainerInterface $container) {
        return new ChangePasswordWithLinkController(
            $container->get(PasswordHasherInterface::class),
            $container->get(UserRepositoryInterface::class),
            $container->get(JWSLoader::class),
            $container->get(Psr7Helper::class)
        );
    },

    'change_password_with_link_validator' => static function (ContainerInterface $container) {
        return new ValidationMiddleware(
            $container->get(Validator::class)->make(
                [],
                [
                    'password' => 'required|min:5',
                    'passwordConfirmed' => 'required|same:password',
                    'token' => 'required|reset-token'
                ]
            ),
            $container->get(Psr7Helper::class)
        );
    },

    ResetPasswordLinkController::class => static function (ContainerInterface $container) {
        return new ResetPasswordLinkController(
            $container->get(Psr7Helper::class),
            $container->get(JWSFactory::class),
            $container->get(ResetMailSender::class),
            $container->get(UserRepositoryInterface::class)
        );
    },

    ResetMailSender::class => static function (ContainerInterface $container) {
        return new ResetMailSender(
            $container->get(MailSender::class),
            $container->get(AppConfig::class)
        );
    },

    'reset_password_link_validator' => static function (ContainerInterface $container) {
        return new ValidationMiddleware(
            $container->get(Validator::class)->make(
                [],
                [
                    'email' => 'required|exists:users,email'
                ]
            ),
            $container->get(Psr7Helper::class)
        );
    },

    PasswordRule::class => static function (ContainerInterface $container) {
        return new PasswordRule(
            $container->get(PasswordHasherInterface::class),
            $container->get(UserManager::class),
            $container->get(UserRepositoryInterface::class)
        );
    },

    UpdatePasswordController::class => static function (ContainerInterface $container) {
        return new UpdatePasswordController(
            $container->get(UserRepositoryInterface::class),
            $container->get(Psr7Helper::class),
            $container->get(PasswordHasherInterface::class),
            $container->get(UserManager::class)
        );
    },

    'update_password_validator' => static function (ContainerInterface $container) {
        return new ValidationMiddleware(
            $container->get(Validator::class)->validate(
                [],
                [
                    'password' => 'required|min:5',
                    'passwordConfirmed' => 'required|same:password',
                    'passwordOld' => 'required|password',
                ]
            ),
            $container->get(Psr7Helper::class)
        );
    },

    UserPresenter::class => static function (ContainerInterface $container) {
        return new UserPresenter(
            $container->get(UserRepositoryInterface::class),
            $container->get(RolesRepositoryInterface::class),
            $container->get(Enforcer::class)
        );
    },

    AlgorithmManager::class => static function () {
        return new AlgorithmManager([new EdDSA()]);
    },

    JWKSet::class => static function (ContainerInterface $container) {
        /** @var AppConfig $config */
        $config = $container->get(AppConfig::class);
        return new JWKSet(
            [
                new JWK(
                    [
                        'use' => 'sig',
                        'alg' => 'EdDSA',
                        'kid' => 'default',
                        'kty' => 'OKP',
                        'crv' => 'Ed25519',
                        'd' => $config->getPrivateKey(),
                        'x' => $config->getPublicKey()
                    ]
                ),
                new JWK(
                    [
                        'use' => 'sig',
                        'alg' => 'EdDSA',
                        'kid' => 'EdDSA-private-sig',
                        'kty' => 'OKP',
                        'crv' => 'Ed25519',
                        'd' => $config->getPrivateKey(),
                        'x' => $config->getPublicKey()
                    ]
                ),
                new JWK(
                    [
                        'alg' => 'EdDSA',
                        'kid' => 'EdDSA-public-sig',
                        'kty' => 'OKP',
                        'crv' => 'Ed25519',
                        'x' => $config->getAuthPulicKey()
                    ]
                )
            ]
        );
    },

    HeaderCheckerManager::class => static function () {
        return new HeaderCheckerManager(
            [new AlgorithmChecker(['EdDSA'])],
            [new JWSTokenSupport()]
        );
    },

    ClaimCheckerManager::class => static function (ContainerInterface $container) {
        $config = $container->get(AppConfig::class);
        return new ClaimCheckerManager(
            [
                new IssuedAtChecker(),
                new NotBeforeChecker(),
                new ExpirationTimeChecker(),
                new MultipleAudienceChecker($config)
            ]
        );
    },

    JWSBuilder::class => static function (ContainerInterface $container) {
        $builder = new JWSBuilder($container->get(AlgorithmManager::class));
        /** @var JWKSet $keys */
        $keys = $container->get(JWKSet::class);
        // Key zero is supposed to be the default key
        $jwk = $keys->get('default');
        return $builder->create()
            ->addSignature($jwk, ['alg' => $jwk->get('alg')]);
    },

    JWSFactory::class => static function (ContainerInterface $container): JWSFactory {
        return new JWSFactory(
            $container->get(JWSBuilder::class),
            $container->get(JWSSerializerManager::class),
            $container->get(AppConfig::class)
        );
    },


    JWSSerializerManager::class => static function () {
        return new JWSSerializerManager([new CompactSerializer()]);
    },

    JWSVerifier::class => static function (ContainerInterface $container) {
        return new JWSVerifier($container->get(AlgorithmManager::class));
    },

    JWSLoader::class => static function (ContainerInterface $container) {
        return new JWSLoader(
            $container->get(\Jose\Component\Signature\JWSLoader::class),
            $container->get(JWKSet::class),
            $container->get(ClaimCheckerManager::class)
        );
    },

    Jose\Component\Signature\JWSLoader::class => static function (ContainerInterface $container) {
        return new Jose\Component\Signature\JWSLoader(
            $container->get(JWSSerializerManager::class),
            $container->get(JWSVerifier::class),
            $container->get(HeaderCheckerManager::class)
        );
    },

    Psr7Helper::class => static fn(ContainerInterface $container) => $container->get(JsonPsr7Helper::class),

    RolesRepositoryInterface::class => static function (ContainerInterface $container) {
        return new RolesRepository($container->get(PDO::class), $container->get(UserFactory::class));
    },

    UserFactory::class => static function () {
        return new UserFactory();
    },

    AuthorizationMiddleware::class => static function (ContainerInterface $container) {
        return new AuthorizationMiddleware(
            $container->get(Enforcer::class),
            $container->get(UserManager::class)
        );
    }
];
