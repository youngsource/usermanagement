<?php
declare(strict_types=1);


namespace Laudis\UserManagement\Databags;


final class AppConfig
{
    private string $name;
    private bool $authOn;
    private string $privateKey;
    private string $publicKey;
    private string $authPulicKey;
    private string $mode;
    private array $audiences;
    private string $audience;
    private string $appDomain;

    public function __construct(
        string $name,
        string $privateKey,
        string $publicKey,
        bool $authOn,
        string $authPulicKey,
        string $mode,
        array $audiences,
        string $audience,
        string $appDomain
    ) {
        $this->name = $name;
        $this->authOn = $authOn;
        $this->privateKey = $privateKey;
        $this->publicKey = $publicKey;
        $this->authPulicKey = $authPulicKey;
        $this->mode = $mode;
        $this->audiences = $audiences;
        $this->audience = $audience;
        $this->appDomain = $appDomain;
    }

    public function getAppDomain(): string
    {
        return $this->appDomain;
    }

    public function getAudience(): string
    {
        return $this->audience;
    }

    public function getAudiences(): array
    {
        return $this->audiences;
    }

    public function isAuthOn(): bool
    {
        return $this->authOn;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrivateKey(): string
    {
        return $this->privateKey;
    }

    public function getPublicKey(): string
    {
        return $this->publicKey;
    }

    public function getAuthPulicKey(): string
    {
        return $this->authPulicKey;
    }

    public function getMode(): string
    {
        return $this->mode;
    }
}
