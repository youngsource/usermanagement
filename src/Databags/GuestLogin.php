<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Databags;

use DateTimeInterface;

final class GuestLogin
{
    private DateTimeInterface $validUntil;
    private bool $seenIntroMessage;

    /**
     * GuestLogin constructor.
     */
    public function __construct(DateTimeInterface $validUntil, bool $seenIntroMessage)
    {
        $this->validUntil = $validUntil;
        $this->seenIntroMessage = $seenIntroMessage;
    }

    public function getValidUntil(): DateTimeInterface
    {
        return $this->validUntil;
    }

    public function seenIntroMessage(): bool
    {
        return $this->seenIntroMessage;
    }
}
