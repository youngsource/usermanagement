<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Databags;

use DateTimeInterface;

final class User
{
    private int $id;
    private string $email;
    private DateTimeInterface $createdAt;
    private ?DateTimeInterface $updatedAt;
    private string $userName;
    private ?string $passwordHash;

    public function __construct(
        int $id,
        string $email,
        DateTimeInterface $createdAt,
        ?DateTimeInterface $updatedAt,
        string $userName,
        ?string $passwordHash = null
    ) {
        $this->id = $id;
        $this->email = $email;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->userName = $userName;
        $this->passwordHash = $passwordHash;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function getPasswordHash(): ?string
    {
        return $this->passwordHash;
    }

    public function setPasswordHash(string $passwordHash): void
    {
        $this->passwordHash = $passwordHash;
    }
}
