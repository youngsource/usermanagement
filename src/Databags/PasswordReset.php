<?php
declare(strict_types=1);


namespace Laudis\UserManagement\Databags;


final class PasswordReset
{
    private string $token;
    private bool $used;

    public function __construct(string $token, bool $used)
    {
        $this->token = $token;
        $this->used = $used;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function isUsed(): bool
    {
        return $this->used;
    }
}
