<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use function Laudis\UserManagement\boot_app;

require __DIR__ . '/../vendor/autoload.php';

boot_app()->run();
