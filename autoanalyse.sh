#!/usr/bin/env bash

echo "building container"

docker-compose -f docker-compose.testing.yml build
docker-compose -f docker-compose.testing.yml up -d

STATUS=0;
echo "static analysis on php"
echo "using phpcs"
echo "cd /opt/project && /var/www/.composer/vendor/bin/phpcs" | docker-compose -f docker-compose.testing.yml run php-cli bash
if (( $? != 0 )); then STATUS=1; fi;
echo "using phan"
echo "cd /opt/project && PHAN_ALLOW_XDEBUG=true php /var/www/.composer/vendor/bin/phan --no-progress-bar --disable-cache --output-mode checkstyle" | docker-compose -f docker-compose.testing.yml run php-cli bash
if (( $? != 0 )); then STATUS=1; fi;
echo "done with static analysis"

echo "Checking environment variables"
echo "php vendor/laudis/common/scripts/check_environment.php env-validation.json" | docker-compose -f docker-compose.testing.yml run php-cli bash
if (( $? != 0 )); then STATUS=1; fi;
echo "Done checking environment variables"

if [[ $STATUS -eq 0 ]]
then
    echo "analysis successful"
    exit 0
else
    echo "analysis unsuccessful"
    echo "please check the output above for errors"
    exit 1
fi