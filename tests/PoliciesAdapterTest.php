<?php
/** @noinspection PhpUnhandledExceptionInspection */
declare(strict_types=1);



namespace Laudis\UserManagement\Tests;


use Casbin\Enforcer;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use PDO;
use PHPUnit\Framework\TestCase;

final class PoliciesAdapterTest extends TestCase
{
    private static Enforcer $enforcer;
    private static UserRepositoryInterface $repository;


    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        rollback_all();
        migrate_all();
        $container = make_container();
        $pdo = $container->get(PDO::class);

        $pdo->exec("INSERT INTO users (email, username, password_hash) VALUES ('alice@test.be', 'alice', 'abc'), ('bob@test.be', 'bob', 'abc'), ('admin@test.be', 'admin', 'abc')");
        $uid1 = $pdo->query("SELECT id FROM users WHERE email = 'alice@test.be'")->fetchColumn(0);
        $uid2 = $pdo->query("SELECT id FROM users WHERE email = 'bob@test.be'")->fetchColumn(0);
        $uid3 = $pdo->query("SELECT id FROM users WHERE email = 'admin@test.be'")->fetchColumn(0);
        $pdo->exec("INSERT INTO roles (name) VALUES ('data1_admin'), ('data2_admin')");
        $roleId1 = $pdo->query("SELECT id FROM roles WHERE name = 'data1_admin'")->fetchColumn(0);
        $roleId2 = $pdo->query("SELECT id FROM roles WHERE name = 'data2_admin'")->fetchColumn(0);
        $pdo->exec("INSERT INTO user_roles (user_id, role_id) VALUES ($uid1, $roleId1), ($uid2, $roleId2), ($uid3, 1)");
        $pdo->exec("INSERT INTO request_policies (role_id, act, path, data) VALUES 
           ($roleId1, 'read', '/data1', '.*'), 
           ($roleId1, 'write', '/data1', '.*'), 
           ($roleId2, 'read', '/data2', '.*'), 
           ($roleId2, 'write', '/data2', '.*')
        ");

        self::$repository = $container->get(UserRepositoryInterface::class);

        self::$enforcer = $container->get(Enforcer::class);
    }

    public function testSimple(): void
    {
        $user = self::$repository->findUser('alice@test.be');
        self::$enforcer->loadFilteredPolicy($user);
        self::assertTrue(self::$enforcer->enforce('user' . $user->getId(), 'read', '/data1', ''));
    }

    public function testAdmin(): void
    {
        $user = self::$repository->findUser('admin@test.be');
        self::$enforcer->loadFilteredPolicy($user);
        self::assertTrue(self::$enforcer->enforce('user' . $user->getId(), 'read', '/data2', ''));
    }

    public function testNestedFail(): void
    {
        $user = self::$repository->findUser('bob@test.be');
        self::$enforcer->loadFilteredPolicy($user);
        self::assertTrue(self::$enforcer->enforce('user' . $user->getId(), 'read', '/data2', ''));
        self::assertFalse(self::$enforcer->enforce('user' . $user->getId(), 'read', '/data1', ''));
    }
}