<?php /** @noinspection PhpUnhandledExceptionInspection */

namespace Laudis\UserManagement\Tests\Middleware;

use DateTime;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\JWS\JWSFactory;
use Laudis\UserManagement\JWS\JWSLoader;
use Laudis\UserManagement\Middleware\AddPossibleUserMiddleware;
use Laudis\UserManagement\UserManager;
use Laudis\UserManagement\UserPresenter;
use Nyholm\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\App;

use function getenv;
use function Laudis\UserManagement\Tests\boot_app;

class AddPossibleUserMiddlewareTest extends TestCase
{
    private static ContainerInterface $container;
    private static App $app;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $app = boot_app(true, true);
        self::$app = $app;
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        self::$container = $app->getContainer();
        self::$app->add(AddPossibleUserMiddleware::class);
    }

    public function testAuthNull(): void
    {
        self::$app->handle(new ServerRequest('GET', '/user', ['AUTHORIZATION' => 'null']));
        self::assertTrue(true);
    }

    public function testAuthActualNull(): void
    {
        self::assertEquals(401, self::$app->handle(new ServerRequest('GET', '/user'))->getStatusCode());
    }

    public function testInvalidToken(): void
    {
        $repo = self::$container->get(UserRepositoryInterface::class);
        $user = $repo->getAllUsers()[0];
        $repo->makeOrUpdateGuestLogin($user, new DateTime('-1 day'));
        $token = self::$container->get(JWSFactory::class)->make($user, null, array_merge(
                self::$container->get(UserPresenter::class)->presentForToken($user),
                ['type' => 'auth'])
        );

        self::assertEquals(401, self::$app->handle(new ServerRequest('GET', '/user', ['AUTHORIZATION' => $token]))->getStatusCode());
    }

    public function testExpiredToken(): void
    {
        $repo = self::$container->get(UserRepositoryInterface::class);
        $user = $repo->getAllUsers()[0];
        $presenter = self::$container->get(UserPresenter::class);
        $token = self::$container->get(JWSFactory::class)
            ->make(
                $user,
                getenv('APP_AUDIENCE'),
                array_merge(
                    $presenter->presentForToken($user),
                    [
                        'exp' => (new DateTime('-1 hour'))->getTimestamp(),
                        'type' => 'auth'
                    ]
                )
            );

        $request = $this->createMock(ServerRequestInterface::class);
        $request->method('getHeaderLine')->willReturn($token);
        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method('handle')->willReturn($this->createMock(ResponseInterface::class));

        self::$container->get(AddPossibleUserMiddleware::class)->process($request, $handler);
        self::assertTrue(true);
    }

    public function testExpiredTokenFalse(): void
    {
        $repo = self::$container->get(UserRepositoryInterface::class);
        $user = $repo->getAllUsers()[0];
        $presenter = self::$container->get(UserPresenter::class);
        $token = self::$container->get(JWSFactory::class)
            ->make(
                $user,
                getenv('APP_AUDIENCE'),
                array_merge(
                    $presenter->presentForToken($user),
                    [
                        'exp' => (new DateTime('-1 hour'))->getTimestamp(),
                        'type' => 'auth'
                    ]
                )
            );

        $request = $this->createMock(ServerRequestInterface::class);
        $request->method('getHeaderLine')->willReturn($token);
        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method('handle')->willReturn($this->createMock(ResponseInterface::class));

        $middleware = new AddPossibleUserMiddleware(
            self::$container->get(UserManager::class),
            self::$container->get(JWSLoader::class),
            false
        );
        $middleware->process($request, $handler);
        self::assertNull(null);
    }
}
