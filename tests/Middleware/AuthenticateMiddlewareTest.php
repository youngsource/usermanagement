<?php
/** @noinspection PhpUnhandledExceptionInspection */
declare(strict_types=1);
/** @noinspection PhpUnhandledExceptionInspection */

namespace Laudis\UserManagement\Tests\Middleware;

use DateTime;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Middleware\AddPossibleUserMiddleware;
use Nyholm\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Slim\App;
use function Laudis\UserManagement\Tests\authentication_token;
use function Laudis\UserManagement\Tests\boot_app;

final class AuthenticateMiddlewareTest extends TestCase
{
    private static ContainerInterface $container;
    private static App $app;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $app = boot_app(true, true);
        self::$app = $app;
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        self::$container = $app->getContainer();
        self::$app->addRoutingMiddleware();
        self::$app->add(AddPossibleUserMiddleware::class);
    }

    public function testProcessNotLoggedIn(): void
    {
        static::assertEquals(401, self::$app->handle(new ServerRequest('GET', '/user'))->getStatusCode());
    }

    public function testProcessGuestNotLoggedIn(): void
    {
        $repo = self::$container->get(UserRepositoryInterface::class);
        $user = $repo->getAllUsers()[0];
        $repo->makeOrUpdateGuestLogin($user, new DateTime('-1 day'));

        $request = new ServerRequest('GET', '/user');
        $request = authentication_token(self::$container, $request, $user);
        self::assertEquals(401, self::$app->handle($request)->getStatusCode());
    }

    public function testValid(): void
    {
        $repo = self::$container->get(UserRepositoryInterface::class);
        $user = $repo->getAllUsers()[1];
        $request = new ServerRequest('GET', '/user');
        $request = authentication_token(self::$container, $request, $user);
        self::assertNotNull(self::$app->handle($request));
    }
}
