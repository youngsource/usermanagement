<?php
/** @noinspection PhpUnhandledExceptionInspection */
declare(strict_types=1);



namespace Laudis\UserManagement\Tests\Middleware;

use Laudis\UserManagement\Contracts\RolesRepositoryInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Middleware\AddPossibleUserMiddleware;
use Laudis\UserManagement\Middleware\AuthenticateMiddleware;
use Laudis\UserManagement\Middleware\AuthorizationMiddleware;
use Nyholm\Psr7\ServerRequest;
use PDO;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use RolesSeeder;
use Slim\App;
use function Laudis\UserManagement\Tests\authentication_token;
use function Laudis\UserManagement\Tests\boot_app;
use function Laudis\UserManagement\Tests\migrate_all;
use function Laudis\UserManagement\Tests\rollback_all;
use function Laudis\UserManagement\Tests\seed_database;

final class AuthorizationMiddlewareTest extends TestCase
{
    private static ContainerInterface $container;
    private static App $app;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $app = boot_app(true, true);
        self::$app = $app;
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        self::$container = $app->getContainer();
        self::$app->get('/test', fn ($req, $res) => $res)
            ->add(AuthorizationMiddleware::class)
            ->add(AuthenticateMiddleware::class);
        seed_database(RolesSeeder::class);
    }

    public function testValid(): void
    {
        $repo = self::$container->get(UserRepositoryInterface::class);
        $user = $repo->getAllUsers()[1];
        $rolesRepository = self::$container->get(RolesRepositoryInterface::class);
        $role = $rolesRepository->getAllRoles()[1];
        $rolesRepository->syncRole($user, $role);
        $stmnt = self::$container->get(PDO::class)
            ->prepare('INSERT INTO request_policies (`role_id`, `act`, `path`, `data`) VALUES (?, ?, ?, ?)');
        $stmnt->execute([$role->getId(), 'GET', '/test', '.*']);
        $request = new ServerRequest('GET', '/test');
        $request = authentication_token(self::$container, $request, $user);

        self::assertEquals(200, self::$app->handle($request)->getStatusCode());
    }

    public function testInvalid(): void
    {
        $repo = self::$container->get(UserRepositoryInterface::class);
        $user = $repo->getAllUsers()[2];
        $request = new ServerRequest('GET', '/test');
        $request = authentication_token(self::$container, $request, $user);
        self::assertEquals(403, self::$app->handle($request)->getStatusCode());
    }
}
