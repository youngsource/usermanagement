<?php
/** @noinspection PhpUnhandledExceptionInspection */
declare(strict_types=1);

namespace Laudis\UserManagement\Tests\Repositories;

use Exception;
use Faker\Factory;
use Laudis\UserManagement\Contracts\RolesRepositoryInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Databags\Role;
use Laudis\UserManagement\Databags\User;
use PHPUnit\Framework\TestCase;
use UserSeeder;

use function array_map;
use function count;
use function Laudis\UserManagement\Tests\make_container;
use function Laudis\UserManagement\Tests\migrate_all;
use function Laudis\UserManagement\Tests\rollback_all;
use function Laudis\UserManagement\Tests\seed_database;

final class RolesRepositoryTest extends TestCase
{
    private static RolesRepositoryInterface $repository;

    /**
     * @throws Exception
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        rollback_all();
        migrate_all();
        self::$repository = make_container()->get(RolesRepositoryInterface::class);
        foreach (self::$repository->getAllRoles() as $role) {
            self::$repository->deleteRole($role);
        }
        seed_database(UserSeeder::class);
    }

    public function testInsertRole(): void
    {
        for ($i = 0; $i < 3; ++$i) {
            self::$repository->insertRole((string)$i);
        }
        $roles = self::$repository->getAllRoles();
        self::assertCount(3, $roles);
        self::assertEquals(['0', '1', '2'], array_map(static fn(Role $role) => $role->getName(), $roles));
    }

    /**
     * @depends testInsertRole
     */
    public function testGetRole(): void
    {
        for ($i = 0; $i < 3; ++$i) {
            self::assertEquals((string)($i), self::$repository->getRole((string)$i)->getName());
        }
    }

    /**
     * @depends testInsertRole
     */
    public function testGetAllRoles(): void
    {
        for ($i = 0; $i < 3; ++$i) {
            self::assertEquals((string)$i, self::$repository->getAllRoles(1, $i)[0]->getName());
        }
    }

    /**
     * @throws Exception
     */
    public function provider(): array
    {
        static $tbr = null;
        if ($tbr === null) {
            $tbr = [$this->getRandomUser(), $this->getRandomUser()];
        }

        return $tbr;
    }

    /**
     * @depends testInsertRole
     * @throws Exception
     */
    public function testSyncRole(): void
    {
        do {
            [$user, $otherUser] = $this->provider();
        } while ($user->getId() === $otherUser->getId());
        $role1 = self::$repository->getRole('1');
        $role2 = self::$repository->getRole('2');

        self::assertCount(0, self::$repository->getRolesForUser($user));
        self::assertCount(0, self::$repository->getRolesForUser($otherUser));

        self::$repository->syncRole($user, $role1);

        self::assertCount(1, self::$repository->getRolesForUser($user));
        self::assertEquals('1', self::$repository->getRolesForUser($user)[0]->getName());
        self::assertCount(0, self::$repository->getRolesForUser($otherUser));

        self::$repository->syncRole($user, $role1);

        self::assertCount(1, self::$repository->getRolesForUser($user));
        self::assertEquals('1', self::$repository->getRolesForUser($user)[0]->getName());
        self::assertCount(0, self::$repository->getRolesForUser($otherUser));

        self::$repository->syncRole($user, $role2);

        self::assertCount(2, self::$repository->getRolesForUser($user));
        self::assertEquals('1', self::$repository->getRolesForUser($user)[0]->getName());
        self::assertEquals('2', self::$repository->getRolesForUser($user)[1]->getName());
        self::assertCount(0, self::$repository->getRolesForUser($otherUser));
    }

    public function testGetRoleNull(): void
    {
        self::assertNull(self::$repository->getRole('abcddkae;rjakghj;a'));
    }

    /**
     * @depends testSyncRole
     * @throws Exception
     */
    public function testDetachRole(): void
    {
        [$user, $otherUser] = $this->provider();
        $role2 = self::$repository->getRole('2');

        self::$repository->detachRole($user, $role2);
        self::$repository->detachRole($otherUser, $role2);

        self::assertCount(1, self::$repository->getRolesForUser($user));
        self::assertEquals('1', self::$repository->getRolesForUser($user)[0]->getName());
        self::assertCount(0, self::$repository->getRolesForUser($otherUser));
    }

    /**
     * @depends testInsertRole
     */
    public function testDeleteRole(): void
    {
        self::$repository->deleteRole(self::$repository->getRole('1'));

        $roles = self::$repository->getAllRoles();
        self::assertCount(2, $roles);
        self::assertEquals(['0', '2'], array_map(static fn(Role $role) => $role->getName(), $roles));
    }

    /**
     * @throws Exception
     */
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        rollback_all();
    }

    /**
     * @throws Exception
     */
    private function getRandomUser(): User
    {
        $faker = Factory::create();
        $users = make_container()->get(UserRepositoryInterface::class)->getAllUsers();

        return $users[$faker->numberBetween(0, count($users) - 1)];
    }
}
