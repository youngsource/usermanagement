<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Tests\Repositories;

use DateTime;
use Exception;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Databags\PasswordReset;
use Laudis\UserManagement\Databags\User;
use Laudis\UserManagement\Repositories\UserRepository;
use Laudis\UserManagement\UserFactory;
use PDO;
use PHPUnit\Framework\TestCase;

use function Laudis\UserManagement\Tests\make_container;
use function Laudis\UserManagement\Tests\migrate_all;
use function Laudis\UserManagement\Tests\rollback_all;

final class UserRepositoryTest extends TestCase
{
    private static UserRepository $repository;
    private static UserFactory $factory;

    /**
     * @throws Exception
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        $container = make_container();

        self::$repository = $container->get(UserRepositoryInterface::class);
        self::$factory = $container->get(UserFactory::class);

        rollback_all();
        migrate_all();
    }

    public function testConstruct(): void
    {
        $pdo = $this->createMock(PDO::class);
        self::assertEquals(new UserRepository($pdo, new UserFactory()), new UserRepository($pdo, new UserFactory()));
    }

    /**
     * @depends testInsertUser
     * @throws Exception
     */
    public function testGetUserById(): void
    {
        self::assertNull(self::$repository->getUserById(0));
        for ($i = 0; $i < 3; ++$i) {
            $user = self::$repository->getUserById($i + 1);
            self::assertNotNull($user);
            self::assertEquals("test$i@pm.me", $user->getEmail());
            self::assertEquals("username$i", $user->getUserName());
            self::assertEquals("abc$i", $user->getPasswordHash());
            self::assertLessThan(new DateTime(), $user->getCreatedAt());
            self::assertNull($user->getUpdatedAt());
            self::assertEquals($i + 1, $user->getId());
        }
    }

    public function testInsertUser(): void
    {
        for ($i = 0; $i < 3; ++$i) {
            self::$repository->insertUser("test$i@pm.me", "username$i", "abc$i");
        }
        self::assertTrue(true);
    }

    /**
     * @depends testMakeGuestLogin
     * @throws Exception
     */
    public function testSetUserSeenMessage(): void
    {
        $user1 = self::$repository->findUser('test1@pm.me');
        self::assertNotNull($user1);
        $user2 = self::$repository->findUser('test2@pm.me');
        self::assertNotNull($user2);

        self::$repository->makeOrUpdateGuestLogin($user2, new DateTime());

        self::$repository->setUserSeenMessage($user1, true);
        self::assertTrue(self::$repository->getGuestLogin($user1)->seenIntroMessage());
        self::assertFalse(self::$repository->getGuestLogin($user2)->seenIntroMessage());

        self::$repository->setUserSeenMessage($user1, false);
        self::assertFalse(self::$repository->getGuestLogin($user1)->seenIntroMessage());
        self::assertFalse(self::$repository->getGuestLogin($user2)->seenIntroMessage());
    }

    /**
     * @depends testInsertUser
     * @throws Exception
     */
    public function testMakeGuestLogin(): void
    {
        $user = self::$repository->findUser('test1@pm.me');
        self::assertNotNull($user);
        self::$repository->makeOrUpdateGuestLogin($user, new DateTime());
        self::assertTrue(true);
    }

    /**
     * @depends testMakeGuestLogin
     * @throws Exception
     */
    public function testGetGuestLogin(): void
    {
        $user = self::$repository->findUser('test1@pm.me');
        self::assertNotNull($user);

        $login = self::$repository->getGuestLogin($user);

        self::assertNotNull($user);
        self::assertFalse($login->seenIntroMessage());
        self::assertLessThan(new DateTime(), $login->getValidUntil());

        self::$repository->makeOrUpdateGuestLogin($user, new DateTime());
        $user = self::$repository->findUser('test1@pm.me');
        self::assertNotNull($user);
        self::assertFalse($login->seenIntroMessage());
        self::assertLessThan(new DateTime(), $login->getValidUntil());

        $user = self::$repository->findUser('test0@pm.me');
        self::assertNull(self::$repository->getGuestLogin($user));
    }

    /**
     * @throws Exception
     * @depends testInsertUser
     */
    public function testFindUser(): void
    {
        self::assertNull(self::$repository->findUser('testt@pm.me'));
        for ($i = 0; $i < 3; ++$i) {
            $user = self::$repository->findUser("test$i@pm.me");
            self::assertNotNull($user);
            self::assertEquals("test$i@pm.me", $user->getEmail());
            self::assertEquals("username$i", $user->getUserName());
            self::assertEquals("abc$i", $user->getPasswordHash());
            self::assertLessThan(new DateTime(), $user->getCreatedAt());
            self::assertNull($user->getUpdatedAt());
            self::assertEquals($i + 1, $user->getId());
        }
    }

    /**
     * @depends testInsertUser
     * @throws Exception
     */
    public function testAddPasswordReset(): void
    {
        $user = self::$repository->getUserById(2);
        self::$repository->addPasswordReset($user, 'a');
        self::$repository->addPasswordReset($user, 'b');
        self::assertTrue(true);
    }

    /**
     * @depends testAddPasswordReset
     * @throws Exception
     */
    public function testGetPasswordResets(): void
    {
        $user = self::$repository->getUserById(1);
        self::assertNotNull($user);
        self::assertCount(0, self::$repository->getPasswordResets($user));
        $user = self::$repository->getUserById(2);
        self::assertNotNull($user);
        $resets = self::$repository->getPasswordResets($user);
        self::assertCount(2, $resets);
        self::assertEquals([false, false], array_map(fn(PasswordReset $reset) => $reset->isUsed(), $resets));
        self::assertEquals(['a', 'b'], array_map(fn(PasswordReset $reset) => $reset->getToken(), $resets));
    }


    /**
     * @depends testAddPasswordReset
     * @throws Exception
     */
    public function testUsePasswordResets(): void
    {
        $user3 = self::$repository->getUserById(3);
        self::$repository->addPasswordReset($user3, 'c');

        $user2 = self::$repository->getUserById(2);
        self::$repository->usePasswordResets($user2);
        $resets = self::$repository->getPasswordResets($user2);
        self::assertEquals([true, true], array_map(fn(PasswordReset $reset) => $reset->isUsed(), $resets));

        $resets = self::$repository->getPasswordResets($user3);
        self::assertEquals([false], array_map(fn(PasswordReset $reset) => $reset->isUsed(), $resets));
    }

    /**
     * @depends testInsertUser
     */
    public function testGetAllUsers(): void
    {
        $users = self::$repository->getAllUsers();
        self::assertCount(3, $users);
        self::assertEquals([1, 2, 3], array_map(fn(User $user) => $user->getId(), $users));

        for ($i = 0; $i < 3; ++$i) {
            $users = self::$repository->getAllUsers(1, $i);
            self::assertCount(1, $users);
            self::assertEquals([$i + 1], array_map(fn(User $user) => $user->getId(), $users));
        }
    }

    /**
     * @throws Exception
     * @depends testInsertUser
     */
    public function testUpdateUser(): void
    {
        $user = self::$repository->getUserById(1);
        $user2 = self::$repository->getUserById(2);
        $user3 = self::$repository->getUserById(3);
        self::assertNotNull($user);
        $user->setPasswordHash('a');
        self::$repository->updateUser($user);
        $newUser = self::$repository->getUserById(1);
        $newUser2 = self::$repository->getUserById(2);
        $newUser3 = self::$repository->getUserById(3);
        self::assertNotNull($newUser->getUpdatedAt());
        self::assertLessThan(new DateTime(), $newUser->getUpdatedAt());
        self::assertEquals('a', $newUser->getPasswordHash());

        self::assertEquals($user->getId(), $newUser->getId());
        self::assertEquals($user->getCreatedAt(), $newUser->getCreatedAt());
        self::assertEquals($user->getEmail(), $newUser->getEmail());
        self::assertEquals($user->getUserName(), $newUser->getUserName());
        self::assertEquals($user2, $newUser2);
        self::assertEquals($user3, $newUser3);
    }


    /**
     * @throws Exception
     * @depends testInsertUser
     */
    public function testDeleteUser(): void
    {
        self::$repository->deleteUser(self::$repository->findUser('test0@pm.me'));
        self::assertNull(self::$repository->findUser('test0@pm.me'));
        self::assertNotNull(self::$repository->findUser('test1@pm.me'));
        self::assertNotNull(self::$repository->findUser('test2@pm.me'));
    }

    /**
     * @throws Exception
     */
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();

        rollback_all();
    }
}
