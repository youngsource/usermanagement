<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Tests;

use Laudis\UserManagement\PasswordHasher;
use PHPUnit\Framework\TestCase;

class PasswordHasherTest extends TestCase
{
    private PasswordHasher $hasher;

    protected function setUp(): void
    {
        parent::setUp();
        $this->hasher = new PasswordHasher();
    }

    public function testHashValid(): void
    {
        self::assertIsString($this->hasher->hash('test'));
        self::assertIsString($this->hasher->hash("\xc3\x28"));
        self::assertIsString($this->hasher->hash('test', [PASSWORD_ARGON2_DEFAULT_THREADS => 8]));
    }

    public function testCheck(): void
    {
        for ($i = 0; $i < 2; ++$i) {
            $hash = $this->hasher->hash('test');
            self::assertTrue($this->hasher->check('test', $hash));
            self::assertFalse($this->hasher->check('a', $hash));
        }
    }
}
