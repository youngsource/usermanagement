<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Tests;

use DI\Container;
use Exception;
use Laudis\UserManagement\Controllers\AuthTokenIssueController;
use Laudis\UserManagement\Controllers\ChangePasswordWithLinkController;
use Laudis\UserManagement\Controllers\ResetPasswordLinkController;
use Laudis\UserManagement\Controllers\UpdatePasswordController;
use Laudis\UserManagement\Controllers\UserController;
use Laudis\UserManagement\Middleware\AddPossibleUserMiddleware;
use Laudis\UserManagement\Middleware\AuthenticateMiddleware;
use Nyholm\Psr7\ServerRequest;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Factory\AppFactory;

use stdClass;

use function Laudis\UserManagement\add_global_middleware;
use function Laudis\UserManagement\add_to_router;
use function Laudis\UserManagement\reset_password_email;

final class HelpersTest extends TestCase
{
    public function testGlobalMiddleware(): void
    {
        $middleware = $this->mockMiddleware();

        $container = new Container();
        $container->set(AddPossibleUserMiddleware::class, $middleware);
        $app = AppFactory::createFromContainer($container);


        add_global_middleware($app->getMiddlewareDispatcher());
        $this->expectException(HttpNotFoundException::class);
        $app->run(new ServerRequest('GET', '/'));
    }

    public function testAddToRouter(): void
    {
        $container = new Container();
        $container->set('reset_password_link_validator', $this->mockMiddleware());
        $container->set('change_password_with_link_validator', $this->mockMiddleware());
        $container->set('update_password_validator', $this->mockMiddleware());
        $container->set('token_validator', $this->mockMiddleware());
        $container->set(AuthenticateMiddleware::class, $this->mockMiddleware(3));

        $container->set(AuthTokenIssueController::class, $this->mockInvocableController());
        $container->set(ResetPasswordLinkController::class, $this->mockInvocableController());
        $container->set(ChangePasswordWithLinkController::class, $this->mockInvocableController());
        $container->set(UpdatePasswordController::class, $this->mockInvocableController());

        $controller = $this->partialMock(stdClass::class, ['presentUser', 'introMessage']);
        $controller->expects($this->once())
            ->method('presentUser')
            ->willReturnCallback(static fn(ServerRequestInterface $request, ResponseInterface $response) => $response);


        $container->set(UserController::class, $controller);

        $app = AppFactory::createFromContainer($container);
        add_to_router($app);
        $app->addRoutingMiddleware();

        self::assertNotNull($app->getRouteCollector()->getNamedRoute('issueToken'));
        self::assertNotNull($app->getRouteCollector()->getNamedRoute('changePasswordWithLink'));
        self::assertNotNull($app->getRouteCollector()->getNamedRoute('updatePassword'));
        self::assertNotNull($app->getRouteCollector()->getNamedRoute('resetPasswordLink'));
        self::assertNotNull($app->getRouteCollector()->getNamedRoute('presentUser'));
        self::assertNotNull($app->getRouteCollector()->getNamedRoute('introMessage'));

        $app->run(new ServerRequest('POST', '/token'));
        $app->run(new ServerRequest('POST', '/reset-password'));
        $app->run(new ServerRequest('POST', '/change-password/a'));
        $app->run(new ServerRequest('POST', '/update-password'));

        $app->run(new ServerRequest('GET', '/user'));

        $controller->expects($this->once())
            ->method('introMessage')
            ->willReturnCallback(static fn(ServerRequestInterface $request, ResponseInterface $response) => $response);

        $app->run(new ServerRequest('GET', '/intro-message'));
    }

    /**
     * @throws Exception
     */
    public function testResetPasswordEmail(): void
    {
        $id = uuid();
        $mail = reset_password_email($id);
        self::assertStringContainsString($id, $mail);
    }

    /**
     * @return MockObject|stdClass
     */
    private function mockInvocableController()
    {
        $controller = $this->partialMock(stdClass::class, ['__invoke']);
        $controller->expects($this->once())
            ->method('__invoke')
            ->willReturnCallback(static fn(ServerRequestInterface $request, ResponseInterface $response) => $response);
        return $controller;
    }

    /**
     * @return MockObject|MiddlewareInterface
     */
    private function mockMiddleware(int $times = 1)
    {
        $middleware = $this->createMock(MiddlewareInterface::class);
        $middleware->expects($this->exactly($times))
            ->method('process')
            ->willReturnCallback(static fn($request, RequestHandlerInterface $handler) => $handler->handle($request));
        return $middleware;
    }

    private function partialMock(string $class, array $methods): MockObject
    {
        return $this->getMockBuilder($class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->addMethods($methods)
            ->getMock();
    }
}