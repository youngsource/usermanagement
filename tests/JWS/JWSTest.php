<?php

namespace Laudis\UserManagement\Tests\JWS;

use DateTime;
use Exception;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Databags\User;
use Laudis\UserManagement\JWS\JWSFactory;
use Laudis\UserManagement\JWS\JWSLoader;
use PHPUnit\Framework\TestCase;

use function array_diff_key;
use function Laudis\UserManagement\Tests\make_container;
use function Laudis\UserManagement\Tests\migrate_all;
use function Laudis\UserManagement\Tests\rollback_all;

final class JWSTest extends TestCase
{
    private JWSFactory $factory;
    private JWSLoader $loader;
    private PasswordHasherInterface $hasher;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        $container = make_container();
        $this->factory = $container->get(JWSFactory::class);
        $this->loader = $container->get(JWSLoader::class);
        $this->hasher = $container->get(PasswordHasherInterface::class);
    }

    /**
     * @throws Exception
     */
    public function testSimple(): void
    {
        $token = $this->factory->make(
            new User(1, 'ghlen@pm.me', new DateTime(), null, 'ghlen', $this->hasher->hash('test'))
        );

        $response = $this->loader->load($token);

        self::assertEquals(
            [
                'iss' => 'usermanagement-testing',
                'aud' => 'connect',
                'user_id' => 1,
                'type' => 'auth'
            ],
            array_diff_key($response, array_flip(['iat', 'nbf', 'exp', 'jti'])
        ));

        self::assertArrayHasKey('iat', $response);
        self::assertArrayHasKey('exp', $response);
        self::assertArrayHasKey('nbf', $response);
        self::assertArrayHasKey('jti', $response);

        self::assertLessThanOrEqual(time(), $response['iat']);
        self::assertLessThanOrEqual(time(), $response['nbf']);
        self::assertGreaterThan(time(), $response['exp']);
        self::assertEquals($response['iat'] + 3600, $response['exp'], 100);
    }
}
