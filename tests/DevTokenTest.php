<?php /** @noinspection PhpUnhandledExceptionInspection */


namespace Laudis\UserManagement\Tests;


use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Databags\AppConfig;
use Laudis\UserManagement\Databags\Role;
use Laudis\UserManagement\JWS\JWSLoader;
use Laudis\UserManagement\Middleware\AddPossibleUserMiddleware;
use Laudis\UserManagement\UserManager;
use Monolog\Test\TestCase;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\ServerRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use function DI\decorate;
use function explode;
use function ob_get_clean;
use function ob_start;

final class DevTokenTest extends TestCase
{
    public function testIntegration(): void
    {
        ob_start();
        include __DIR__ . '/../resources/commands/generate_dev_token.php';
        [$token, $idToken] = explode("\n", ob_get_clean());


        $container = make_container();

        $container->set(
            AppConfig::class,
            decorate(
                static function (AppConfig $config) {
                    ob_start();
                    include __DIR__ . '/../resources/commands/generate_sign_keypair.php';
                    $keys = ob_get_clean();
                    [$public, $private] = explode("\n", $keys);
                    return new AppConfig(
                        $config->getName(),
                        $private,
                        $public,
                        $config->isAuthOn(),
                        $config->getAuthPulicKey(),
                        $config->getMode(),
                        $config->getAudiences(),
                        'development',
                        $config->getAppDomain()
                    );
                }
            )
        );

        $loader = $container->get(JWSLoader::class);
        self::assertEquals(1, $loader->load($token)['user_id']);

        $request = (new ServerRequest('GET', '/'))->withHeader('Authorization', $token);
        $request = $request->withHeader('Authentication', $idToken);
        $container->get(AddPossibleUserMiddleware::class)->process(
            $request,
            new class () implements RequestHandlerInterface {
                public function handle(ServerRequestInterface $request): ResponseInterface
                {
                    return new Response();
                }
            }
        );
        $manager = $container->get(UserManager::class);
        $user = $manager->getLoggedInUser();
        self::assertEquals(1, $user->getId());
        self::assertEquals([new Role('development', 1)], $manager->getRoles());
        self::assertTrue($container->get(PasswordHasherInterface::class)->check('dev', $user->getPasswordHash()));
        self::assertEquals('development', $user->getUserName());
        self::assertEqualsWithDelta(time(), $user->getCreatedAt()->getTimestamp(), 1);
        self::assertEqualsWithDelta(time(), $user->getUpdatedAt()->getTimestamp(), 1);

        self::assertNull($manager->getGuestLogin());
    }
}