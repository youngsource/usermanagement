<?php
declare(strict_types=1);

namespace Laudis\UserManagement\Tests\Controllers;

use Exception;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Controllers\AuthTokenIssueController;
use Laudis\UserManagement\JWS\JWSLoader;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\ServerRequest;
use PDO;
use PHPUnit\Framework\TestCase;
use Slim\App;

use function array_flip;
use function array_intersect_key;
use function DI\decorate;
use function DI\get;
use function Laudis\UserManagement\Tests\boot_app;
use function Laudis\UserManagement\Tests\make_container;
use function Laudis\UserManagement\Tests\rollback_all;
use function safe_json_decode;
use function sleep;
use function time;

final class AuthTokenIssueControllerTest extends TestCase
{
    private static App $app;
    private JWSLoader $loader;

    /**
     * @throws Exception
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$app = boot_app(true, true);

        $container = self::$app->getContainer();
        $hasher = $container->get(PasswordHasherInterface::class);
        $container->get(UserRepositoryInterface::class)->insertUser('test@pm.me', 'ghlen', $hasher->hash('test'));
        $container->get(UserRepositoryInterface::class)->insertUser('test2@pm.me', 'ghlen', $hasher->hash('test'));
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->loader = self::$app->getContainer()->get(JWSLoader::class);
    }

    /**
     * @throws Exception
     */
    public function testTemporaryAccount(): void
    {
        /** @var PDO $pdo */
        $pdo = self::$app->getContainer()->get(PDO::class);

        $pdo->exec('INSERT INTO temporary_logins (user_id, valid_until, viewed_intro_message) VALUES (11, \'2019-03-10 02:55:05\', 0)');
        $pdo->exec('INSERT INTO temporary_logins (user_id, valid_until, viewed_intro_message) VALUES (12, \'2100-03-10 02:55:05\', 0)');


        $request = new ServerRequest('POST', '/token');
        $request = $request->withParsedBody(['email' => 'test@pm.me', 'password' => 'test', 'aud' => 'practinet']);
        $response = self::$app->handle($request);

        self::assertEquals(422, $response->getStatusCode());
        $body = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        self::assertArrayHasKey('errors', $body);
        self::assertArrayHasKey('password', $body['errors']);
        self::assertContains('Uw login is vervallen, gelieve te verlengen', $body['errors']['password']);


        $request = new ServerRequest('POST', '/token');
        $request = $request->withParsedBody(['email' => 'test2@pm.me', 'password' => 'test', 'aud' => 'practinet']);
        $response = self::$app->handle($request);

        self::assertEquals(200, $response->getStatusCode());
    }

    /**
     * @throws Exception
     */
    public function testIdTokenConsistency(): void
    {
        $container = self::$app->getContainer();

        $request = new ServerRequest('POST', '/token');
        $request = $request->withParsedBody(['email' => 'test@pm.me', 'password' => 'test', 'aud' => 'practinet']);
        $response1 = $container->get(AuthTokenIssueController::class)->__invoke($request, new Response());
        sleep(2);
        $response2 = $container->get(AuthTokenIssueController::class)->__invoke($request, new Response());

        self::assertEquals(
            safe_json_decode((string)$response1->getBody(), 512, JSON_THROW_ON_ERROR)['authenticationToken'],
            safe_json_decode((string)$response2->getBody(), 512, JSON_THROW_ON_ERROR)['authenticationToken']
        );
    }

    /**
     * @throws Exception
     */
    public function testDifferentTokensForDifferentUsers(): void
    {
        $container = self::$app->getContainer();

        $request = new ServerRequest('POST', '/token');
        $request = $request->withParsedBody(['email' => 'test@pm.me', 'password' => 'test', 'aud' => 'practinet']);
        $response1 = $container->get(AuthTokenIssueController::class)->__invoke($request, new Response());

        $request = new ServerRequest('POST', '/token');
        $request = $request->withParsedBody(['email' => 'test2@pm.me', 'password' => 'test', 'aud' => 'practinet']);
        $response2 = $container->get(AuthTokenIssueController::class)->__invoke($request, new Response());

        self::assertNotEquals(
            safe_json_decode((string)$response1->getBody(), 512, JSON_THROW_ON_ERROR)['authenticationToken'],
            safe_json_decode((string)$response2->getBody(), 512, JSON_THROW_ON_ERROR)['authenticationToken']
        );
    }

    /**
     * @throws Exception
     */
    public function testIdTokenKeys(): void
    {
        $container = self::$app->getContainer();

        $request = new ServerRequest('POST', '/token');
        $request = $request->withParsedBody(['email' => 'test@pm.me', 'password' => 'test', 'aud' => 'practinet']);
        $response = $container->get(AuthTokenIssueController::class)->__invoke($request, new Response());

        $tokens = safe_json_decode((string)$response->getBody(), 512, JSON_THROW_ON_ERROR);
        self::assertArrayHasKey('authenticationToken', $tokens);


        $token = $this->loader->load($tokens['authenticationToken']);

        self::assertArrayHasKey('type', $token);
        self::assertArrayHasKey('iat', $token);
        self::assertArrayHasKey('nbf', $token);
        self::assertArrayHasKey('exp', $token);
        self::assertArrayHasKey('iss', $token);
        self::assertArrayHasKey('aud', $token);
        self::assertArrayHasKey('user_id', $token);
        self::assertArrayHasKey('jti', $token);

        self::assertEquals('laudis-authentication', $token['type']);
        self::assertEquals(0, $token['iat']);
        self::assertEquals(0, $token['nbf']);
        self::assertEquals(PHP_INT_MAX, $token['exp']);
        self::assertEquals('laudis', $token['iss']);
        self::assertEquals('all', $token['aud']);
        $id = $container->get(UserRepositoryInterface::class)->findUser('test@pm.me')->getId();
        self::assertEquals($id, $token['user_id']);
        self::assertEquals($id, $token['jti']);
    }

    /**
     * @throws Exception
     */
    public function testToken(): void
    {
        $container = self::$app->getContainer();

        $request = new ServerRequest('POST', '/token');
        $request = $request->withParsedBody(['email' => 'test@pm.me', 'password' => 'test', 'aud' => 'practinet']);
        $response = $container->get(AuthTokenIssueController::class)->__invoke($request, new Response());

        self::assertEquals(200, $response->getStatusCode());
        $contents = (string)$response->getBody();
        self::assertJson($contents);
        $token = $this->loader->load(safe_json_decode($contents)['token']);
        self::assertIsArray($token);

        self::assertEquals(
            [
                'iss' => 'usermanagement-testing',
                'aud' => 'practinet',
                'user_id' => $container->get(UserRepositoryInterface::class)->findUser('test@pm.me')->getId(),
                'type' => 'auth'
            ],
            array_intersect_key($token, array_flip(['iss', 'aud', 'user_id', 'type'])
            ));

        self::assertArrayHasKey('iat', $token);
        self::assertArrayHasKey('exp', $token);
        self::assertArrayHasKey('nbf', $token);
        self::assertArrayHasKey('jti', $token);

        self::assertArrayHasKey('email', $token);
        self::assertArrayNotHasKey('password_hash', $token);
        self::assertArrayHasKey('created_at', $token);
        self::assertArrayHasKey('updated_at', $token);
        self::assertArrayHasKey('username', $token);
        self::assertArrayHasKey('guest_login', $token);
        self::assertArrayHasKey('roles', $token);

        self::assertLessThanOrEqual(time(), $token['iat']);
        self::assertLessThanOrEqual(time(), $token['nbf']);
        self::assertGreaterThan(time(), $token['exp']);
        self::assertEqualsWithDelta($token['iat'] + 3600, $token['exp'], 100.0);
    }

    /**
     * @throws Exception
     */
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        rollback_all();
    }
}
