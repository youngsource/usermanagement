<?php

namespace Laudis\UserManagement\Tests\Controllers;

use DateTime;
use DI\Container;
use Exception;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Controllers\ChangePasswordWithLinkController;
use Laudis\UserManagement\Controllers\ResetPasswordLinkController;
use Laudis\UserManagement\JWS\JWSFactory;
use Laudis\UserManagement\JWS\JWSLoader;
use Mailgun\Api\Message;
use Mailgun\Mailgun;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Slim\App;

use UnexpectedValueException;

use function DI\decorate;
use function Laudis\UserManagement\Tests\boot_app;
use function Laudis\UserManagement\Tests\migrate_all;
use function Laudis\UserManagement\Tests\rollback_all;

class ResetPasswordLinkControllerTest extends TestCase
{

    private static App $app;
    private UserRepositoryInterface $repository;
    private JWSLoader $loader;

    /**
     * @throws Exception
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$app = boot_app(true, true);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = self::$app->getContainer()->get(UserRepositoryInterface::class);
        $this->loader = self::$app->getContainer()->get(JWSLoader::class);
    }

    /**
     * @throws Exception
     */
    public function testSimple(): void
    {
        /** @var Container $container */
        $container = self::$app->getContainer();
        $container->set(Mailgun::class, decorate(function () {
            $messages = $this->createMock(Message::class);
            $messages->expects($this->once())->method('send');
            $mailgun = $this->createMock(Mailgun::class);
            $mailgun->expects($this->once())->method('messages')->willReturn($messages);
            return $mailgun;
        }));

        self::$app->addRoutingMiddleware();

        $request = new ServerRequest('POST', '/reset-password');
        $user = $this->repository->getAllUsers()[0];
        $request = $request->withParsedBody(['email' => $user->getEmail()]);

        $response = self::$app->handle($request);

        self::assertEquals(200, $response->getStatusCode());

        $resets = $this->repository->getPasswordResets($user);
        self::assertCount(1, $resets);

        $reset = $resets[0];
        self::assertFalse($reset->isUsed());
        $token = $this->loader->load($reset->getToken());

        self::assertArrayHasKey('email', $token);
        self::assertArrayHasKey('exp', $token);
        self::assertArrayHasKey('type', $token);
        self::assertEquals($user->getEmail(), $token['email']);
        self::assertLessThanOrEqual((new DateTime('+1 day'))->getTimestamp(), $token['exp']);
        self::assertEquals('reset', $token['type']);
    }

    /**
     * @throws Exception
     */
    public function testFail(): void
    {
        $controller = self::$app->getContainer()->get(ResetPasswordLinkController::class);

        $request = new ServerRequest('POST', '/reset-password');
        $request = $request->withParsedBody(['email' => 'a']);

        $this->expectException(UnexpectedValueException::class);
        $controller->__invoke($request, new Response());
    }
}
