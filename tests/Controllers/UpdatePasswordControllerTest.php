<?php

namespace Laudis\UserManagement\Tests\Controllers;

use Exception;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Middleware\AddPossibleUserMiddleware;
use Nyholm\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Slim\App;

use function Laudis\UserManagement\Tests\authentication_token;
use function Laudis\UserManagement\Tests\boot_app;
use function Laudis\UserManagement\Tests\rollback_all;

final class UpdatePasswordControllerTest extends TestCase
{
    private static App $app;
    private UserRepositoryInterface $repository;
    private PasswordHasherInterface $hasher;

    /**
     * @throws Exception
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$app = boot_app(true, true);
        self::$app->addRoutingMiddleware();
        self::$app->add(AddPossibleUserMiddleware::class);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = self::$app->getContainer()->get(UserRepositoryInterface::class);
        $this->hasher = self::$app->getContainer()->get(PasswordHasherInterface::class);
    }

    /**
     * @throws Exception
     */
    public function testSimple(): void
    {
        $user = $this->repository->getAllUsers()[0];
        $user->setPasswordHash($this->hasher->hash('abcdefg'));
        $this->repository->updateUser($user);
        $request = new ServerRequest('POST', '/update-password');
        $request = authentication_token(self::$app->getContainer(), $request, $user);
        $request = $request->withParsedBody(
            ['password' => 'abcdef', 'passwordConfirmed' => 'abcdef', 'passwordOld' => 'abcdefg']
        );

        $response = self::$app->handle($request);

        self::assertEquals(200, $response->getStatusCode());
        $user = $this->repository->findUser($user->getEmail());
        self::assertNotNull($user);
        self::assertTrue($this->hasher->check('abcdef', $user->getPasswordHash()));
    }
}
