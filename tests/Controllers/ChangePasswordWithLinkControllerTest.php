<?php

namespace Laudis\UserManagement\Tests\Controllers;

use Exception;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Controllers\ChangePasswordWithLinkController;
use Laudis\UserManagement\JWS\JWSFactory;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Slim\App;

use UnexpectedValueException;

use function Laudis\UserManagement\Tests\boot_app;

class ChangePasswordWithLinkControllerTest extends TestCase
{
    private static App $app;
    private ChangePasswordWithLinkController $controller;
    private UserRepositoryInterface $repository;
    private JWSFactory $factory;
    private PasswordHasherInterface $hasher;

    /**
     * @throws Exception
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$app = boot_app(true, true);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->controller = self::$app->getContainer()->get(ChangePasswordWithLinkController::class);
        $this->repository = self::$app->getContainer()->get(UserRepositoryInterface::class);
        $this->factory = self::$app->getContainer()->get(JWSFactory::class);
        $this->hasher = self::$app->getContainer()->get(PasswordHasherInterface::class);
    }

    /**
     * @throws Exception
     */
    public function testSimple(): void
    {
        $user = $this->repository->getAllUsers()[0];
        $token = $this->factory->make($user, null, ['email' => $user->getEmail(), 'type' => 'reset']);
        $this->repository->addPasswordReset($user, $token);

        $response = new Response();
        $request = new ServerRequest('GET', '/');
        $request = $request->withParsedBody(['password' => 'a', 'token' => $token]);

        $response = $this->controller->__invoke($request, $response);

        self::assertEquals(200, $response->getStatusCode());

        $user = $this->repository->findUser($user->getEmail());
        self::assertTrue($this->hasher->check('a', $user->getPasswordHash()));
        self::assertCount(1, $this->repository->getPasswordResets($user));
        self::assertTrue($this->repository->getPasswordResets($user)[0]->isUsed());
    }

    /**
     * @throws Exception
     */
    public function testWrong(): void
    {
        $user = $this->repository->getAllUsers()[0];
        $token = $this->factory->make($user, null, ['email' => '', 'type' => 'reset']);

        $response = new Response();
        $request = new ServerRequest('GET', '/');
        $request = $request->withParsedBody(['password' => 'a', 'token' => $token]);

        $this->expectException(UnexpectedValueException::class);
        $this->controller->__invoke($request, $response);
    }
}
