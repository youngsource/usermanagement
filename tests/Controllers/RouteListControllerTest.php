<?php
/** @noinspection PhpUnhandledExceptionInspection */
declare(strict_types=1);

namespace Laudis\UserManagement\Tests\Controllers;

use Monolog\Test\TestCase;
use Nyholm\Psr7\ServerRequest;
use Slim\App;
use function count;
use function Laudis\UserManagement\Tests\boot_app;

final class RouteListControllerTest extends TestCase
{
    private static App $app;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$app = boot_app(false, false);
    }

    public function testList(): void
    {
        $response = self::$app->handle(new ServerRequest('GET', '/routes'));
        self::assertEquals(200, $response->getStatusCode());
        $array = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        self::assertCount(count(self::$app->getRouteCollector()->getRoutes()), $array);
        self::assertEquals(array_values($array), $array);

        foreach($array as $item) {
            $route = self::$app->getRouteCollector()->getNamedRoute($item['name']);
            self::arrayHasKey('name');
            self::assertEquals($item['name'], $route->getName());
            self::arrayHasKey('methods');
            self::assertEquals($item['methods'], $route->getMethods());
            self::arrayHasKey('arguments');
            self::assertEquals($item['arguments'], $route->getArguments());
            self::arrayHasKey('identifier');
            self::assertEquals($item['identifier'], $route->getIdentifier());
            self::arrayHasKey('pattern');
            self::assertEquals($item['pattern'], $route->getPattern());
        }
    }
}
