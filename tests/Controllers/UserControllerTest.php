<?php

namespace Laudis\UserManagement\Tests\Controllers;

use DateTime;
use Exception;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Middleware\AddPossibleUserMiddleware;
use Laudis\UserManagement\UserPresenter;
use Nyholm\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Slim\App;

use function Laudis\UserManagement\Tests\authentication_token;
use function Laudis\UserManagement\Tests\boot_app;
use function safe_json_decode;

class UserControllerTest extends TestCase
{
    private static App $app;
    private UserRepositoryInterface $repository;
    private UserPresenter $presenter;

    /**
     * @throws Exception
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$app = boot_app(true, true);
        self::$app->addRoutingMiddleware();
        self::$app->add(AddPossibleUserMiddleware::class);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = self::$app->getContainer()->get(UserRepositoryInterface::class);
        $this->presenter = self::$app->getContainer()->get(UserPresenter::class);
    }

    /**
     * @throws Exception
     */
    public function testPresentUser(): void
    {
        $user = $this->repository->getAllUsers()[0];

        $request = new ServerRequest('GET', '/user');
        $request = authentication_token(self::$app->getContainer(), $request, $user);
        $response = self::$app->handle($request);

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals($this->presenter->present($user), safe_json_decode((string)$response->getBody()));
    }

    /**
     * @throws Exception
     */
    public function testIntroMessage(): void
    {
        $user = $this->repository->getAllUsers()[0];
        $this->repository->makeOrUpdateGuestLogin($user, (new DateTime('+1 day')));

        $request = new ServerRequest('GET', '/intro-message');
        $request = authentication_token(self::$app->getContainer(), $request, $user);

        $response = self::$app->handle($request);

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals(true, $this->repository->getGuestLogin($user)->seenIntroMessage());
    }
}
