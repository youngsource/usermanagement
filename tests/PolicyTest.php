<?php /** @noinspection PhpUnhandledExceptionInspection */


namespace Laudis\Nodes\Tests;


use Casbin\Enforcer;
use FastRoute\Dispatcher\GroupCountBased;
use FastRoute\RouteCollector;
use PHPUnit\Framework\TestCase;
use function count;
use function FastRoute\simpleDispatcher;
use function Laudis\UserManagement\make_container;
use function Laudis\UserManagement\Tests\boot_app;

final class PolicyTest extends TestCase
{
    private Enforcer $enforcer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->enforcer = new Enforcer(__DIR__ . '/../resources/config/model.conf', __DIR__ . '/policy.csv');
        $this->enforcer->addFunction('keyMatch4', make_container()->get('keyMatch4'));
    }

    public function testSimpleSuccess(): void
    {
        $policies = [
            ['user1', 'GET', '/test/1', '{}'],
            ['user1', 'GET', '/test/1', '{"category":1}'],
            ['user1', 'GET', '/test/1', '{"category":3}'],
            ['user1', 'GET', '/test', '{"category":3}'],
            ['user1', 'GET', '/ABC', '{"category":3}'],
            ['null', 'GET', '/', '{}'],
            ['null', 'GET', '/test/1', '{}'],
            ['root', 'GET', '/test/1', '{}'],
            ['root', 'POST', '/test', ' {}']
        ];

        $results = [
            false,
            false,
            true,
            false,
            false,
            true,
            false,
            true,
            true
        ];

        for ($i = 0, $iMax = count($policies); $i < $iMax; ++$i) {
            self::assertEquals($results[$i], $this->enforcer->enforce(...$policies[$i]), $i);
        }
    }
}