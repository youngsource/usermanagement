<?php /** @noinspection EfferentObjectCouplingInspection */

declare(strict_types=1);

namespace Laudis\UserManagement\Tests;

use Casbin\Enforcer;
use Casbin\Persist\Adapter;
use DI\Container;
use DI\DependencyException;
use DI\NotFoundException;
use Exception;
use Jose\Component\Checker\ClaimCheckerManager;
use Jose\Component\Checker\HeaderCheckerManager;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\JWKSet;
use Jose\Component\Signature\JWSLoader;
use Jose\Component\Signature\Serializer\JWSSerializerManager;
use Jose\Component\Signature\JWSBuilder;
use Laudis\Common\Contracts\Psr7Helper;
use Laudis\Common\Middleware\ValidationMiddleware;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\RolesRepositoryInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Controllers\AuthTokenIssueController;
use Laudis\UserManagement\Controllers\ChangePasswordWithLinkController;
use Laudis\UserManagement\Controllers\ResetPasswordLinkController;
use Laudis\UserManagement\Controllers\UpdatePasswordController;
use Laudis\UserManagement\Controllers\UserController;
use Laudis\UserManagement\Databags\AppConfig;
use Laudis\UserManagement\JWS\JWSFactory;
use Laudis\UserManagement\Middleware\AddPossibleUserMiddleware;
use Laudis\UserManagement\Middleware\AuthenticateMiddleware;
use Laudis\UserManagement\ResetMailSender;
use Laudis\UserManagement\Rules\ExistsRule;
use Laudis\UserManagement\UserFactory;
use Laudis\UserManagement\UserManager;
use Laudis\UserManagement\UserPresenter;
use Laudis\UserManagement\Validation\PasswordRule;
use Laudis\UserManagement\Validation\ResetTokenRule;
use Laudis\UserManagement\Validation\ValidAudienceRule;
use PHPUnit\Framework\ExpectationFailedException;
use PHPUnit\Framework\TestCase;
use SebastianBergmann\RecursionContext\InvalidArgumentException;
use Slim\Interfaces\RouteParserInterface;

final class RegisterTest extends TestCase
{
    private Container $container;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->container = make_container();
    }

    /**
     * @throws DependencyException
     * @throws NotFoundException
     * @throws \PHPUnit\Framework\Exception
     * @throws ExpectationFailedException
     * @throws InvalidArgumentException
     */
    public function testKeys(): void
    {
        self::assertInstanceOf(UserRepositoryInterface::class, $this->container->get(UserRepositoryInterface::class));
        self::assertInstanceOf(PasswordHasherInterface::class, $this->container->get(PasswordHasherInterface::class));
        self::assertInstanceOf(AuthTokenIssueController::class, $this->container->get(AuthTokenIssueController::class));
        self::assertInstanceOf(ValidationMiddleware::class, $this->container->get('token_validator'));
        self::assertInstanceOf(UserController::class, $this->container->get(UserController::class));
        self::assertInstanceOf(
            AddPossibleUserMiddleware::class,
            $this->container->get(AddPossibleUserMiddleware::class)
        );
        self::assertInstanceOf(AppConfig::class, $this->container->get(AppConfig::class));
        self::assertInstanceOf(UserManager::class, $this->container->get(UserManager::class));
        self::assertInstanceOf(AuthenticateMiddleware::class, $this->container->get(AuthenticateMiddleware::class));
        self::assertInstanceOf(Enforcer::class, $this->container->get(Enforcer::class));
        self::assertInstanceOf(ResetTokenRule::class, $this->container->get(ResetTokenRule::class));
        self::assertInstanceOf(
            ChangePasswordWithLinkController::class,
            $this->container->get(ChangePasswordWithLinkController::class)
        );
        self::assertInstanceOf(
            ValidationMiddleware::class,
            $this->container->get('change_password_with_link_validator')
        );
        self::assertInstanceOf(
            ResetPasswordLinkController::class,
            $this->container->get(ResetPasswordLinkController::class)
        );
        self::assertInstanceOf(ValidationMiddleware::class, $this->container->get('reset_password_link_validator'));
        self::assertInstanceOf(PasswordRule::class, $this->container->get(PasswordRule::class));
        self::assertInstanceOf(UpdatePasswordController::class, $this->container->get(UpdatePasswordController::class));
        self::assertInstanceOf(ValidationMiddleware::class, $this->container->get('update_password_validator'));
        self::assertInstanceOf(UserPresenter::class, $this->container->get(UserPresenter::class));
        self::assertInstanceOf(AlgorithmManager::class, $this->container->get(AlgorithmManager::class));
        self::assertInstanceOf(JWKSet::class, $this->container->get(JWKSet::class));
        self::assertInstanceOf(HeaderCheckerManager::class, $this->container->get(HeaderCheckerManager::class));
        self::assertInstanceOf(ClaimCheckerManager::class, $this->container->get(ClaimCheckerManager::class));
        self::assertInstanceOf(JWSBuilder::class, $this->container->get(JWSBuilder::class));
        self::assertInstanceOf(JWSFactory::class, $this->container->get(JWSFactory::class));
        self::assertInstanceOf(JWSSerializerManager::class, $this->container->get(JWSSerializerManager::class));
        self::assertInstanceOf(
            \Laudis\UserManagement\JWS\JWSLoader::class,
            $this->container->get(\Laudis\UserManagement\JWS\JWSLoader::class)
        );
        self::assertInstanceOf(JWSLoader::class, $this->container->get(JWSLoader::class));
        self::assertInstanceOf(Psr7Helper::class, $this->container->get(Psr7Helper::class));
        self::assertInstanceOf(RolesRepositoryInterface::class, $this->container->get(RolesRepositoryInterface::class));
        self::assertInstanceOf(ResetMailSender::class, $this->container->get(ResetMailSender::class));
        self::assertInstanceOf(ValidAudienceRule::class, $this->container->get(ValidAudienceRule::class));
        self::assertInstanceOf(UserFactory::class, $this->container->get(UserFactory::class));
    }
}
