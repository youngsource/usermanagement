<?php /** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace Laudis\UserManagement\Tests;

use DateTime;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Databags\GuestLogin;
use Laudis\UserManagement\Databags\User;
use Laudis\UserManagement\Exceptions\UnauthenticatedException;
use Laudis\UserManagement\UserFactory;
use Laudis\UserManagement\UserManager;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class UserManagerTest extends TestCase
{
    /**
     * @var UserRepositoryInterface|MockObject
     */
    private $repository;
    private UserManager $manager;
    private User $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->manager = new UserManager(new UserFactory());
        $this->user = new User(1, 'a', new DateTime(), new DateTime(), 'd', 'e');
    }

    public function testGetLoggedInUser(): void
    {
        $this->manager->login(
            [
                'user_id' => $this->user->getId(),
                'email' => $this->user->getEmail(),
                'username' => $this->user->getUserName(),
                'roles' => [],
                'created_at' => '@' . $this->user->getCreatedAt()->getTimestamp(),
                'updated_at' => '@' . $this->user->getUpdatedAt()->getTimestamp(),
                'password_hash' => $this->user->getPasswordHash(),
                'guest_login' => null
            ]
        );
        self::assertEqualsWithDelta($this->user, $this->manager->getLoggedInUser(), 1);
    }

    public function testGetLoggedInUserStrictFail(): void
    {
        $this->expectException(UnauthenticatedException::class);
        $this->manager->getLoggedInUserStrict();
    }

    public function testGetLoggedInUserStrictFail1(): void
    {
        $this->expectException(UnauthenticatedException::class);
        $this->manager->getGuestLogin();
    }

    public function testGetLoggedInUserStrictFail2(): void
    {
        $this->expectException(UnauthenticatedException::class);
        $this->manager->getRoles();
    }

    public function testGetLoggedInUserStrict(): void
    {
        $this->manager->login(
            [
                'user_id' => $this->user->getId(),
                'email' => $this->user->getEmail(),
                'username' => $this->user->getUserName(),
                'roles' => [],
                'created_at' => '@' . $this->user->getCreatedAt()->getTimestamp(),
                'updated_at' => '@' . $this->user->getUpdatedAt()->getTimestamp(),
                'password_hash' => $this->user->getPasswordHash(),
                'guest_login' => null
            ]
        );

        self::assertEqualsWithDelta($this->user, $this->manager->getLoggedInUserStrict(), 1);
    }

    public function testGetGuestLoginFail(): void
    {
        $this->expectException(UnauthenticatedException::class);
        self::assertNull($this->manager->getGuestLogin());
    }

    public function testGetGuestLogin(): void
    {
        $login = new GuestLogin(new DateTime(), false);
        $this->manager->login(
            [
                'user_id' => $this->user->getId(),
                'email' => $this->user->getEmail(),
                'username' => $this->user->getUserName(),
                'roles' => [],
                'created_at' => '@' . $this->user->getCreatedAt()->getTimestamp(),
                'updated_at' => '@' . $this->user->getUpdatedAt()->getTimestamp(),
                'password_hash' => $this->user->getPasswordHash(),
                'guest_login' => ['valid_until' => $login->getValidUntil()->format('Y-m-d'), 'seen_intro_message' => false],
            ]
        );

        self::assertEqualsWithDelta($login, $this->manager->getGuestLogin(), 10000000);
    }
}
