<?php /** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace Laudis\UserManagement\Tests;

use Casbin\Enforcer;
use DateTime;
use Laudis\UserManagement\Contracts\RolesRepositoryInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Databags\GuestLogin;
use Laudis\UserManagement\Databags\Role;
use Laudis\UserManagement\Databags\User;
use Laudis\UserManagement\UserPresenter;
use PHPUnit\Framework\TestCase;

final class UserPresenterTest extends TestCase
{
    public function testUserPresenterNoGuestLogin(): void
    {
        $rolesRepo = $this->createMock(RolesRepositoryInterface::class);
        $at = new DateTime();
        $user = new User(1, 'a', $at, $at, 'd', 'e');
        $rolesRepo->expects(static::once())
            ->method('getRolesForUser')
            ->with($user)
            ->willReturn([new Role('a', 1), new Role('b', 2)]);

        $userRepo = $this->createMock(UserRepositoryInterface::class);
        $userRepo->expects(static::once())
            ->method('getGuestLogin')
            ->with($user)
            ->willReturn(null);


        $enforcer = new Enforcer(__DIR__ . '/../resources/config/model.conf', __DIR__ . '/policy.csv');
        $userPresenter = new UserPresenter($userRepo, $rolesRepo, $enforcer);
        self::assertEquals(
            [
                'email' => $user->getEmail(),
                'id' => $user->getId(),
                'username' => $user->getUserName(),
                'roles' => [
                    [
                        'name' => 'a',
                        'id' => 1
                    ],
                    [
                        'name' => 'b',
                        'id' => 2
                    ]
                ],
                'guestLogin' => null,
                'createdAt' => $at->getTimestamp(),
                'updatedAt' => $at->getTimestamp(),
                'authorization' => $enforcer->getModel()['p']['p']->policy,
            ],
            $userPresenter->present($user)
        );
    }

    public function testUserToken(): void
    {
        $rolesRepo = $this->createMock(RolesRepositoryInterface::class);
        $at = new DateTime();
        $user = new User(1, 'a', $at, $at, 'd', 'e');
        $rolesRepo->expects(static::once())
            ->method('getRolesForUser')
            ->with($user)
            ->willReturn([new Role('a', 1), new Role('b', 2)]);

        $userRepo = $this->createMock(UserRepositoryInterface::class);
        $userRepo->expects(static::once())
            ->method('getGuestLogin')
            ->with($user)
            ->willReturn(null);

        $enforcer = new Enforcer(__DIR__ . '/../resources/config/model.conf', __DIR__ . '/policy.csv');
        $userPresenter = new UserPresenter(
            $userRepo,
            $rolesRepo,
            $enforcer
        );
        self::assertEquals(
            [
                'email' => $user->getEmail(),
                'username' => $user->getUserName(),
                'roles' => [
                    [
                        'name' => 'a',
                        'id' => 1
                    ],
                    [
                        'name' => 'b',
                        'id' => 2
                    ]
                ],
                'guest_login' => null,
                'created_at' => '@' . $at->getTimestamp(),
                'updated_at' => '@' . $at->getTimestamp(),
                'authorization' => $enforcer->getModel()['p']['p']->policy
            ],
            $userPresenter->presentForToken($user)
        );
    }

    public function testUserPresenterWithGuestLogin(): void
    {
        $rolesRepo = $this->createMock(RolesRepositoryInterface::class);
        $at = new DateTime();
        $user = new User(1, 'a', $at, $at, 'd', 'e');
        $rolesRepo->expects(static::once())
            ->method('getRolesForUser')
            ->with($user)
            ->willReturn([]);

        $userRepo = $this->createMock(UserRepositoryInterface::class);
        $userRepo->expects(static::once())
            ->method('getGuestLogin')
            ->with($user)
            ->willReturn(new GuestLogin($at, true));

        $policy = new Enforcer(__DIR__ . '/../resources/config/model.conf', __DIR__ . '/policy.csv');
        $userPresenter = new UserPresenter(
            $userRepo,
            $rolesRepo,
            $policy
        );
        self::assertEquals(
            [
                'email' => $user->getEmail(),
                'id' => $user->getId(),
                'username' => $user->getUserName(),
                'roles' => [],
                'guestLogin' => [
                    'validUntil' => $at->format('Y-m-d'),
                    'seenIntroMessage' => true
                ],
                'createdAt' => $at->getTimestamp(),
                'updatedAt' => $at->getTimestamp(),
                'authorization' => $policy->getModel()['p']['p']->policy
            ],
            $userPresenter->present($user)
        );
    }
}
