<?php

declare(strict_types=1);

namespace Laudis\UserManagement\Tests;

use DateTime;
use Exception;
use Laudis\UserManagement\UserFactory;
use PHPUnit\Framework\TestCase;

final class UserFactoryTest extends TestCase
{
    private UserFactory $factory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->factory = new UserFactory();
    }

    /**
     * @throws Exception
     */
    public function testRoleFromAssociativeArray(): void
    {
        $array = [
            'id' => 1,
            'name' => 'test'
        ];

        $role = $this->factory->roleFromAssociativeArray($array);

        self::assertEquals(1, $role->getId());
        self::assertEquals('test', $role->getName());
    }

    /**
     * @throws Exception
     */
    public function testUserFromAssociativeArray(): void
    {
        $array = [
            'id' => 1,
            'email' => 'ghlen@pm.me',
            'created_at' => '@1170288000',
            'updated_at' => null,
            'username' => 'ghlen',
            'password_hash' => 'abc'
        ];

        $user = $this->factory->userFromAssociativeArray($array);

        self::assertEquals(1, $user->getId());
        self::assertEquals('ghlen@pm.me', $user->getEmail());
        self::assertEquals(new DateTime('@1170288000'), $user->getCreatedAt());
        self::assertNull($user->getUpdatedAt());
        self::assertEquals('ghlen', $user->getUserName());
        self::assertEquals('abc', $user->getPasswordHash());
    }

    /**
     * @throws Exception
     */
    public function testGuestLoginFromAssociativeArray(): void
    {

        $array = [
            'valid_until' => '@1170288000',
            'viewed_intro_message' => true
        ];

        $role = $this->factory->guestLoginFromAssociativeArray($array);

        self::assertEquals(new DateTime('@1170288000'), $role->getValidUntil());
        self::assertEquals(true, $role->seenIntroMessage());
    }
}
