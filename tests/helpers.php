<?php
declare(strict_types=1);

namespace Laudis\UserManagement\Tests;

use DI\Container;
use DI\ContainerBuilder;
use Exception;
use Laudis\UserManagement\Controllers\AuthTokenIssueController;
use Laudis\UserManagement\CustomJsonErrorRenderer;
use Laudis\UserManagement\Databags\User;
use Laudis\UserManagement\ErrorHandler;
use Laudis\UserManagement\Middleware\AddPossibleUserMiddleware;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\ServerRequest;
use Phinx\Console\PhinxApplication;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Factory\AppFactory;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;
use UserSeeder;

use function Laudis\UserManagement\add_to_router;
use function safe_json_decode;

/**
 * @throws Exception
 */
function migrate_all(): void
{
    $phinx = new PhinxApplication();
    $phinx->setAutoExit(false);

    $phinxfile = __DIR__ . '/../phinx.php';
    $phinx->run(new StringInput("migrate -c $phinxfile"), new NullOutput());
}

/**
 * @throws Exception
 */
function seed_database(string $seeder): void
{
    $phinx = new PhinxApplication();
    $phinx->setAutoExit(false);

    $phinxfile = __DIR__ . '/../phinx.php';
    $phinx->run(new StringInput("seed:run -c $phinxfile -s $seeder"), new NullOutput());
}

/**
 * @throws Exception
 */
function rollback_all(): void
{
    $phinx = new PhinxApplication();
    $phinx->setAutoExit(false);

    $phinxfile = __DIR__ . '/../phinx.php';
    $phinx->run(new StringInput("rollback -t 0 -c $phinxfile"), new NullOutput());
}

/**
 * @throws Exception
 */
function make_container(): Container
{
    $builder = new ContainerBuilder();
    $builder->useAutowiring(false);
    $builder->useAnnotations(false);
    $container = $builder->addDefinitions(
        __DIR__ . '/../vendor/laudis/common/src/register.php',
        __DIR__ . '/../src/register.php'
    )->build();
    $container->set('project_root', __DIR__ . '/../');

    return $container;
}

/**
 * @throws Exception
 */
function boot_app(bool $seed, bool $migrate = true): App
{
    if ($migrate) {
        rollback_all();
        migrate_all();
    }
    if ($seed) {
        seed_database(UserSeeder::class);
    }
    $container = make_container();
    $app = AppFactory::createFromContainer($container);
    $middleware = $app->addErrorMiddleware(true, true, true);
    $handler = new ErrorHandler(
        $app->getCallableResolver(),
        $app->getResponseFactory(),
        $container->get(LoggerInterface::class)
    );
    $errorRenderer = new CustomJsonErrorRenderer();
    $handler->registerErrorRenderer('application/json', $errorRenderer);
    $handler->setDefaultErrorRenderer('application/json', $errorRenderer);
    $handler->forceContentType('application/json');
    $middleware->setDefaultErrorHandler($handler);
    $container->set(App::class, static fn () => $app);
    $app->addRoutingMiddleware();
    add_to_router($app);

    $app->add(AddPossibleUserMiddleware::class);
    return $app;
}

/**
 * @throws Exception
 */
function authentication_token(ContainerInterface $container, ServerRequestInterface $request, User $user): ServerRequestInterface
{
    $controller = $container->get(AuthTokenIssueController::class);
    $tokenRequest = new ServerRequest('GET', '/');
    $tokenRequest = $tokenRequest->withParsedBody(['email' => $user->getEmail()]);
    $response = $controller->__invoke($tokenRequest, new Response());
    $data = safe_json_decode((string)$response->getBody());
    $token =  $data['token'];
    $laudisToken =  $data['authenticationToken'];

    /** @noinspection CallableParameterUseCaseInTypeContextInspection */
    $request = $request->withHeader('AUTHORIZATION', $token);
    /** @noinspection CallableParameterUseCaseInTypeContextInspection */
    $request = $request->withHeader('AUTHENTICATION', $laudisToken);

    return $request;
}