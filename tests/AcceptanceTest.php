<?php /** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace Laudis\UserManagement\Tests;


use DI\Container;
use Exception;
use Laudis\Common\Exceptions\ValidationException;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Controllers\AuthTokenIssueController;
use Laudis\UserManagement\Databags\AppConfig;
use Laudis\UserManagement\Databags\User;
use Laudis\UserManagement\JWS\JWSFactory;
use Laudis\UserManagement\JWS\JWSLoader;
use Laudis\UserManagement\UserManager;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use UserSeeder;

use function array_merge;
use function safe_json_decode;

final class AcceptanceTest extends TestCase
{
    private static Container $container;
    private static UserRepositoryInterface $repository;
    private static string $resetToken;
    private ?string $token = null;
    private static User $user;

    /**
     * @throws Exception
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        rollback_all();
        migrate_all();
        seed_database(UserSeeder::class);
    }

    protected function setUp(): void
    {
        parent::setUp();
        self::$container = make_container();
        self::$repository = self::$container->get(UserRepositoryInterface::class);
        $user = self::$repository->getAllUsers()[0];
        $user->setPasswordHash(self::$container->get(PasswordHasherInterface::class)->hash('abc'));
        self::$repository->updateUser($user);
        self::$user = $user;
        self::$resetToken = self::$container->get(JWSFactory::class)->make(
            self::$user,
            self::$container->get(AppConfig::class)->getAudience(),
            ['email' => self::$user->getEmail(), 'type' => 'reset']
        );
        self::$repository->addPasswordReset($user, self::$resetToken);
    }

    public function testToken(): void
    {
        $validator = $this->getValidator('token_validator');
        $this->expectException(ValidationException::class);
        $validator->process($this->request(['email' => null]), $this->handler());
    }

    public function testToken1(): void
    {
        $validator = $this->getValidator('token_validator');
        $this->expectException(ValidationException::class);
        $validator->process($this->request(['email' => 'ghlen@pm.me']), $this->handler());
    }

    public function testToken2(): void
    {
        $validator = $this->getValidator('token_validator');
        $this->expectException(ValidationException::class);
        $validator->process($this->request(['email' => self::$user->getEmail()]), $this->handler());
    }

    public function testToken3(): void
    {
        $validator = $this->getValidator('token_validator');
        $this->expectException(ValidationException::class);
        $validator->process($this->request(['email' => self::$user->getEmail(), 'password' => null]), $this->handler());
    }

    public function testToken4(): void
    {
        $validator = $this->getValidator('token_validator');
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request(['email' => self::$user->getEmail(), 'password' => 'ab']),
            $this->handler()
        );
    }

    public function testToken5(): void
    {
        $response = $this->getValidator('token_validator')->process(
            $this->request(
                [
                    'email' => self::$user->getEmail(),
                    'password' => 'abc',
                    'aud' => self::$container->get(AppConfig::class)->getAudience()
                ]
            ),
            $this->handler()
        );
        self::assertNotNull($response);
    }

    public function testToken6(): void
    {
        $validator = $this->getValidator('token_validator');
        $this->expectException(ValidationException::class);
        $response = $validator->process(
            $this->request(
                [
                    'password' => 'abc',
                    'aud' => self::$container->get(AppConfig::class)->getAudience()
                ]
            ),
            $this->handler()
        );
        self::assertNotNull($response);
    }

    public function testChangePassword(): void
    {
        $validator = $this->getValidator('change_password_with_link_validator');
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request([]),
            $this->handler()
        );
    }

    public function testChangePassword1(): void
    {
        $validator = $this->getValidator('change_password_with_link_validator');
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request(['password' => null]),
            $this->handler()
        );
    }

    public function testChangePassword2(): void
    {
        $validator = $this->getValidator('change_password_with_link_validator');
        $token = self::$container->get(JWSFactory::class)->make(
            self::$user,
            self::$container->get(AppConfig::class)->getAudience(),
            ['email' => self::$user->getEmail(), 'type' => 'reset']
        );
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request(['password' => 'abcd', 'passwordConfirmed' => 'abcd', 'token' => $token]),
            $this->handler()
        );
    }


    public function testChangePassword3(): void
    {
        $validator = $this->getValidator('change_password_with_link_validator');
        $token = self::$container->get(JWSFactory::class)->make(
            self::$user,
            self::$container->get(AppConfig::class)->getAudience(),
            ['email' => self::$user->getEmail(), 'type' => 'reset']
        );
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request(['password' => 'abcde', 'passwordConfirmed' => 'abcdf', 'token' => $token]),
            $this->handler()
        );
    }


    public function testChangePassword4(): void
    {
        $validator = $this->getValidator('change_password_with_link_validator');
        $token = self::$container->get(JWSFactory::class)->make(
            self::$user,
            self::$container->get(AppConfig::class)->getAudience(),
            ['email' => self::$user->getEmail(), 'type' => 'reset']
        );
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request(['password' => 'abcde', 'passwordConfirmed' => 'abcde', 'token' => $token]),
            $this->handler()
        );
    }


    public function testChangePassword5(): void
    {
        $validator = $this->getValidator('change_password_with_link_validator');
        $token = self::$container->get(JWSFactory::class)->make(
            self::$user,
            self::$container->get(AppConfig::class)->getAudience(),
            ['email' => 'ghlen@Pm.me', 'type' => 'reset']
        );
        $this->expectException(ValidationException::class);
        self::assertNotNull(
            $validator->process(
                $this->request(['password' => 'abcde', 'passwordConfirmed' => 'abcde', 'token' => $token]),
                $this->handler()
            )
        );
    }


    public function testChangePassword6(): void
    {
        $validator = $this->getValidator('change_password_with_link_validator');
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request(['password' => 'abcde', 'passwordConfirmed' => 'abcde', 'token' => 'abc']),
            $this->handler()
        );
    }


    public function testChangePassword7(): void
    {
        $validator = $this->getValidator('change_password_with_link_validator');
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request(['password' => 'abcde', 'passwordConfirmed' => 'abcde']),
            $this->handler()
        );
    }

    public function testChangePassword8(): void
    {
        $validator = $this->getValidator('change_password_with_link_validator');
        self::assertNotNull(
            $validator->process(
                $this->request(['password' => 'abcde', 'passwordConfirmed' => 'abcde', 'token' => self::$resetToken]),
                $this->handler()
            )
        );
    }

    public function testChangePassword9(): void
    {
        $validator = $this->getValidator('change_password_with_link_validator');
        $token = self::$container->get(JWSFactory::class)->make(
            self::$user,
            self::$container->get(AppConfig::class)->getAudience(),
            ['email' => 'ghlen@pm.me', 'type' => 'der']
        );
        $this->expectException(ValidationException::class);
        self::assertNotNull(
            $validator->process(
                $this->request(['password' => 'abcde', 'passwordConfirmed' => 'abcde', 'token' => $token]),
                $this->handler()
            )
        );
    }

    public function testChangePassword10(): void
    {
        $validator = $this->getValidator('change_password_with_link_validator');
        self::$repository->usePasswordResets(self::$user);
        $this->expectException(ValidationException::class);
        self::assertNotNull(
            $validator->process(
                $this->request(['password' => 'abcde', 'passwordConfirmed' => 'abcde', 'token' => self::$resetToken]),
                $this->handler()
            )
        );
    }

    public function testResetPasswordLink(): void
    {
        $validator = $this->getValidator('reset_password_link_validator');
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request([]),
            $this->handler()
        );
    }

    public function testResetPasswordLink1(): void
    {
        $validator = $this->getValidator('reset_password_link_validator');
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request(['email' => null]),
            $this->handler()
        );
    }

    public function testResetPasswordLink2(): void
    {
        $validator = $this->getValidator('reset_password_link_validator');
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request(['email' => 'abc']),
            $this->handler()
        );
    }

    public function testResetPasswordLink3(): void
    {
        $validator = $this->getValidator('reset_password_link_validator');
        self::assertNotNull(
            $validator->process(
                $this->request(['email' => self::$user->getEmail()]),
                $this->handler()
            )
        );
    }

    public function testUpdatePasswordValidator(): void
    {
        $validator = $this->getValidator('update_password_validator');
        $this->login();
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request([]),
            $this->handler()
        );
    }

    public function testUpdatePasswordValidator1(): void
    {
        $validator = $this->getValidator('update_password_validator');
        $this->login();
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request(['password' => 'abcd', 'passwordConfirmed' => 'abcd', 'passwordOld' => 'abc']),
            $this->handler()
        );
    }

    public function testUpdatePasswordValidator2(): void
    {
        $validator = $this->getValidator('update_password_validator');
        $this->login();
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request(['password' => 'abcde', 'passwordConfirmed' => 'abcdef', 'passwordOld' => 'abc']),
            $this->handler()
        );
    }

    public function testUpdatePasswordValidator3(): void
    {
        $validator = $this->getValidator('update_password_validator');
        $this->login();
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request(['password' => 'abcd', 'passwordConfirmed' => 'abcd', 'passwordOld' => 'blergh']),
            $this->handler()
        );
    }

    public function testUpdatePasswordValidator4(): void
    {
        $validator = $this->getValidator('update_password_validator');
        $this->login();
        self::assertNotNull(
            $validator->process(
                $this->request(['password' => 'abcde', 'passwordConfirmed' => 'abcde', 'passwordOld' => 'abc']),
                $this->handler()
            )
        );
    }

    public function testUpdatePasswordValidator5(): void
    {
        $validator = $this->getValidator('update_password_validator');
        $this->login();
        $this->expectException(ValidationException::class);
        self::assertNotNull(
            $validator->process(
                $this->request(
                    ['email' => 'abcd', 'password' => 'abcde', 'passwordConfirmed' => 'abcde', 'passwordOld' => 'abc']
                ),
                $this->handler()
            )
        );
    }

    public function testUpdatePasswordValidator6(): void
    {
        $validator = $this->getValidator('update_password_validator');
        $this->expectException(ValidationException::class);
        $validator->process(
            $this->request(['password' => 'abcde', 'passwordConfirmed' => 'abcde', 'passwordOld' => 'abc']),
            $this->handler()
        );
    }


    private function handler(bool $called = false): RequestHandlerInterface
    {
        $handler = $this->createMock(RequestHandlerInterface::class);
        if ($called) {
            $handler->expects(self::once())
                ->method('handle')
                ->willReturn($this->createMock(ResponseInterface::class));
        }
        return $handler;
    }

    private function getValidator(string $id): MiddlewareInterface
    {
        return self::$container->get($id);
    }


    private function request(array $body = [], array $header = []): ServerRequestInterface
    {
        if ($this->token) {
            $header = array_merge(['AUTHORIZATION' => $this->token], $header);
        }
        $request = new ServerRequest('GET', '/', $header);
        $request = $request->withParsedBody($body);
        return $request;
    }

    private function login(): void
    {
        $user = self::$user;
        $request = new ServerRequest('GET', '/');
        $request = $request->withParsedBody(
            ['email' => $user->getEmail(), 'aud' => self::$container->get(AppConfig::class)->getAudience()]
        );
        $response = self::$container->get(AuthTokenIssueController::class)->__invoke($request, new Response());
        $this->token = safe_json_decode((string)$response->getBody())['token'];
        self::$container->get(UserManager::class)
            ->login(self::$container->get(JWSLoader::class)->load($this->token));
    }

    private function logout(): void
    {
        $this->token = null;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->logout();
    }
}